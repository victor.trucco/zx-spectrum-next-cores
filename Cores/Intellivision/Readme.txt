You can help support this core becoming a patron
https://www.patreon.com/vtrucco

---------------------------------------------------------------------------------
-- Intellivision for miSTer
-- Grabulosaure 
-- https://github.com/Grabulosaure/Intv_MiSTer
---------------------------------------------------------------------------------
-- Developed with the help of the JZINTV emulator
---------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- ZX Spectrum Next port by Victor Trucco - 2020
--
-------------------------------------------------------------------------------

Spectrum Next Membrane:
- Use the keys Q,A,O,P as directional and M, N and Space as fire buttons
- Keys 1 to 0 as the numbers on the Intellivision joystick
- Symbol Shift is the CLEAR button.
- RGB and VGA can be toggled with NMI button + "2" key.
- Open/close the loader menu with DRIVE button.

PS/2 keyboard:
- Use the keys A,S,D,W as directional and B, N, M (or Space, CTRL and ALT) as fire buttons. 
- Top keys 1 to 0 as the numbers on the Intellivision joystick
- Keypad (or an external keypad) can be used. ATTENTION! Inverted rows to match the intellivision positions. "789" on keypad are "123" on Intellivision joystick, "456" are the same and "123" is "789". Keypad "0" is the clear key and keypad "." is the "0" key.
- RGB and VGA can be toggled with F2 key.
- You can't use the arrows keys to move to avoid collision with the keypad, use ASDW instead.
- Open/close the loader menu with F12 button.

Changelog

005 - 23/03/2020 - Fix the membrane wrong clock. Speed doubled on Sega joystick protocol.
004 - 22/03/2020 - Support to external PS/2 keypad. Support for Sega 3/6 buttons joypad.
003 - 21/03/2020 - Fix for "2" key on PS/2 keyboard. The key don't change the video to RGB anymore.
002 - 19/03/2020 - Menu now visible on RGB video
001 - 17/03/2020 - Initial

You can help support this core becoming a patron
https://www.patreon.com/vtrucco