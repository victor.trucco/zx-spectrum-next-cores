Odyssey 2 
Port for ZX Spectrum Next and Loader by Victor Trucco 2020 
Original: FPGA Videopac by Arnim Laeuger 2007 

- HDMI
- VGA
- 2 joysticks
- Membrane Keyboard
- PS/2 Keyboard supported, but not needed

"Drive" Button to open the loader and select a game using the joystick. 
"NMI" button to toggle the scanlines.

Attention! 
Know bug: Sometimes you need to press "Drive" button after the boot to read the SD on first time

Some games use the joystick on port 1, others on port 2 and others support both ports.

Special Odyssey 2 Keys:
Reset = CAPS + 1 (or Break or Reset Button)
Clear = CAPS + 0 (or Delete)
Plus  = Symbol + K
Minus = Symbol + J
Divider = Symbol + V
Times = Symbol + B
Equal = Symbom + L
Interrogation mark = Symbol + C 
YES = Y
NO = N

If you like this core, please support at 
https://www.patreon.com/vtrucco

Changelog
002 - 18/04/2020 - Better SD handling
001 - 17/04/2020 - Initial