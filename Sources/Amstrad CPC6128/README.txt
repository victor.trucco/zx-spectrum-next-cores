------------------------------------------------------
-                                                    -
- Ported to ZX Spectrum Next by Victor Trucco - 2018 -
-                                                    -
------------------------------------------------------

There FPGAmstrada has some limited SD card support. 
You can place the DSK files on the SD root and with a 
"CAT" command check the first that was mounted. With 
"Page Up" key, the CPC resets and the next DSK is mounted. 

Load the disks with 

RUN"xxxxx

Where xxxxx is the name showed on CAT command. 



-------------------------------------------------------
# FPGAmstrad
FPGAmstrad source code, running on final platform ZX-Uno, thanks Jepalza !
