------------------------------------------------------------------------------
-- Galaxian Arcade ZX-UNO port by Quest 2017
------------------------------------------------------------------------------
-- FPGA GALAXIAN TOP
--
-- Version  downto  2.50
--
-- Copyright(c) 2004 Katsumi Degawa , All rights reserved
--
-- Important  not
--
-- This program is freeware for non-commercial use.
-- The author does not guarantee this program.
-- You can use this at your own risk.
--
-- 2004- 4-30  galaxian modify by K.DEGAWA
-- 2004- 5- 6  first release.
-- 2004- 8-23  Improvement with T80-IP.
-- 2004- 9-22  The problem which missile didn't sometimes come out from was improved.
------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--
-- ZX Spectrum Next top by Victor Trucco - 2020
--
-------------------------------------------------------------------------------


library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.all;


use work.pkg_galaxian.all;

entity galaxian_top is
	port(
			 -- Clocks
      clock_50_i        : in    std_logic;

      -- SRAM (AS7C34096)
      ram_addr_o        : out   std_logic_vector(18 downto 0)  := (others => '0');
      ram_data_io       : inout std_logic_vector(15 downto 0)  := (others => 'Z');
      ram_oe_n_o        : out   std_logic                      := '1';
      ram_we_n_o        : out   std_logic                      := '1';
      ram_ce_n_o        : out   std_logic_vector( 3 downto 0)  := (others => '1');

      -- PS2
      ps2_clk_io        : inout std_logic                      := 'Z';
      ps2_data_io       : inout std_logic                      := 'Z';
      ps2_pin6_io       : inout std_logic                      := 'Z';  -- Mouse clock
      ps2_pin2_io       : inout std_logic                      := 'Z';  -- Mouse data

      -- SD Card
      sd_cs0_n_o        : out   std_logic                      := '1';
      sd_cs1_n_o        : out   std_logic                      := '1';
      sd_sclk_o         : out   std_logic                      := '0';
      sd_mosi_o         : out   std_logic                      := '0';
      sd_miso_i         : in    std_logic;

      -- Flash
      flash_cs_n_o      : out   std_logic                      := '1';
      flash_sclk_o      : out   std_logic                      := '0';
      flash_mosi_o      : out   std_logic                      := '0';
      flash_miso_i      : in    std_logic;
      flash_wp_o        : out   std_logic                      := '0';
      flash_hold_o      : out   std_logic                      := '1';

      -- Joystick
      joyp1_i           : in    std_logic;
      joyp2_i           : in    std_logic;
      joyp3_i           : in    std_logic;
      joyp4_i           : in    std_logic;
      joyp6_i           : in    std_logic;
      joyp7_o           : out   std_logic                      := '1';
      joyp9_i           : in    std_logic;
      joysel_o          : out   std_logic                      := '0';

      -- Audio
      audioext_l_o      : out   std_logic                      := '0';
      audioext_r_o      : out   std_logic                      := '0';
      audioint_o        : out   std_logic                      := '0';

      -- K7
      ear_port_i        : in    std_logic;
      mic_port_o        : out   std_logic                      := '0';

      -- Buttons
      btn_divmmc_n_i    : in    std_logic;
      btn_multiface_n_i : in    std_logic;
      btn_reset_n_i     : in    std_logic;

      -- Matrix keyboard
      keyb_row_o        : out   std_logic_vector( 7 downto 0)  := (others => 'Z');
      keyb_col_i        : in    std_logic_vector( 6 downto 0);

      -- Bus
      bus_rst_n_io      : inout std_logic                      := 'Z';
      bus_clk35_o       : out   std_logic                      := 'Z';
      bus_addr_o        : out   std_logic_vector(15 downto 0)  := (others => 'Z');
      bus_data_io       : inout std_logic_vector( 7 downto 0)  := (others => 'Z');
      bus_int_n_io      : inout std_logic                      := 'Z';
      bus_nmi_n_i       : in    std_logic;
      bus_ramcs_i       : in    std_logic;
      bus_romcs_i       : in    std_logic;
      bus_wait_n_i      : in    std_logic;
      bus_halt_n_o      : out   std_logic                      := 'Z';
      bus_iorq_n_o      : out   std_logic                      := 'Z';
      bus_m1_n_o        : out   std_logic                      := 'Z';
      bus_mreq_n_o      : out   std_logic                      := 'Z';
      bus_rd_n_o        : out   std_logic                      := 'Z';
      bus_wr_n_o        : out   std_logic                      := 'Z';
      bus_rfsh_n_o      : out   std_logic                      := 'Z';
      bus_busreq_n_i    : in    std_logic;
      bus_busack_n_o    : out   std_logic                      := 'Z';
      bus_iorqula_n_i   : in    std_logic;

      -- VGA
      rgb_r_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_g_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_b_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      hsync_o           : out   std_logic                      := '1';
      vsync_o           : out   std_logic                      := '1';
      csync_o           : out   std_logic                      := 'Z';

      -- HDMI
      hdmi_p_o          : out   std_logic_vector(3 downto 0);
      hdmi_n_o          : out   std_logic_vector(3 downto 0);

      -- I2C (RTC and HDMI)
      i2c_scl_io        : inout std_logic                      := 'Z';
      i2c_sda_io        : inout std_logic                      := 'Z';

      -- ESP
      esp_gpio0_io      : inout std_logic                      := 'Z';
      esp_gpio2_io      : inout std_logic                      := 'Z';
      esp_rx_i          : in    std_logic;
      esp_tx_o          : out   std_logic                      := '1';

      -- PI GPIO
      accel_io          : inout std_logic_vector(27 downto 0)  := (others => 'Z');

      -- Vacant pins
      extras_io         : inout std_logic := 'Z'	
	);
end;

architecture RTL of galaxian_top is
	--
	-- 	HARDWARE SELECTOR
	--		TRUE = galaxian hardware
	-- 	FALSE = mrdo's nightmare
	--
	constant HWSEL_GALAXIAN : 	boolean := FALSE;

	--    CPU ADDRESS BUS
	signal W_A                : std_logic_vector(15 downto 0) := (others => '0');
	--    CPU IF
	signal W_CPU_CLK          : std_logic := '0';
	signal W_CPU_MREQn        : std_logic := '0';
	signal W_CPU_NMIn         : std_logic := '0';
	signal W_CPU_RDn          : std_logic := '0';
	signal W_CPU_RESETn       : std_logic := '0';
	signal W_CPU_RFSHn        : std_logic := '0';
	signal W_CPU_WAITn        : std_logic := '0';
	signal W_CPU_WRn          : std_logic := '0';
	signal W_RESETn           : std_logic := '0';
	-------- CLOCK GEN ---------------------------
	signal W_CLK_12M          : std_logic := '0';
	signal W_CLK_18M          : std_logic := '0';
	signal W_CLK_36M          : std_logic := '0';
	signal W_CLK_6M           : std_logic := '0';
	signal W_CLK_6Mn          : std_logic := '0';
	signal WB_CLK_12M         : std_logic := '0';
	signal WB_CLK_6M          : std_logic := '0';
	-------- H and V COUNTER -------------------------
	signal W_C_BLn            : std_logic := '0';
	signal W_C_BLnX           : std_logic := '0';
	signal W_C_BLX            : std_logic := '0';
	signal W_H_BL             : std_logic := '0';
	signal W_H_SYNC           : std_logic := '0';
	signal W_V_BLn            : std_logic := '0';
	signal W_V_BL2n           : std_logic := '0';
	signal W_V_SYNC           : std_logic := '0';
	signal W_H_CNT            : std_logic_vector(8 downto 0) := (others => '0');
	signal W_V_CNT            : std_logic_vector(7 downto 0) := (others => '0');
	-------- CPU RAM  ----------------------------
	signal W_CPU_RAM_DO       : std_logic_vector(7 downto 0) := (others => '0');
	-------- ADDRESS DECDER ----------------------
	signal W_BD_G             : std_logic := '0';
	signal W_CPU_RAM_CSn      : std_logic := '0';
	signal W_CPU_RAM_RDn      : std_logic := '0';
	signal W_CPU_RAM_WRn      : std_logic := '0';
	signal W_CPU_ROM_CSn      : std_logic := '0';
	signal W_DIP_OEn          : std_logic := '0';
	signal W_H_FLIP           : std_logic := '0';
	signal W_LAMP_WEn         : std_logic := '0';
	signal W_OBJ_RAM_RDn      : std_logic := '0';
	signal W_OBJ_RAM_RQn      : std_logic := '0';
	signal W_OBJ_RAM_WRn      : std_logic := '0';
	signal W_PITCHn           : std_logic := '0';
	signal W_SOUND_WEn        : std_logic := '0';
	signal W_STARS_ON         : std_logic := '0';
	signal W_STARS_OFFn       : std_logic := '0';
	signal W_SW0_OEn          : std_logic := '0';
	signal W_SW1_OEn          : std_logic := '0';
	signal W_V_FLIP           : std_logic := '0';
	signal W_VID_RAM_RDn      : std_logic := '0';
	signal W_VID_RAM_WRn      : std_logic := '0';
	signal W_WDR_OEn          : std_logic := '0';
	--------- INPORT -----------------------------
	signal W_SW_DO            : std_logic_vector( 7 downto 0) := (others => '0');
	--------- VIDEO  -----------------------------
	signal W_VID_DO           : std_logic_vector( 7 downto 0) := (others => '0');
	-----  DATA I/F -------------------------------------
	signal W_CPU_ROM_DO       : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_CPU_ROM_DOB      : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_BDO              : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_BDI              : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_CPU_RAM_CLK      : std_logic := '0';
	signal W_VOL1             : std_logic := '0';
	signal W_VOL2             : std_logic := '0';
	signal W_FIRE             : std_logic := '0';
	signal W_HIT              : std_logic := '0';
	--signal W_FS3              : std_logic := '0';
	--signal W_FS2              : std_logic := '0';
	--signal W_FS1              : std_logic := '0';
	signal W_FS               : std_logic_vector( 2 downto 0) := (others => '0');
	-----  BTTONS  -------------------------------------
	signal C1                 : std_logic := '0';
	signal C2                 : std_logic := '0';
	signal D1                 : std_logic := '0';
	signal D2                 : std_logic := '0';
	signal J1                 : std_logic := '0';
	signal J2                 : std_logic := '0';
	signal L1                 : std_logic := '0';
	signal L2                 : std_logic := '0';
	signal R1                 : std_logic := '0';
	signal R2                 : std_logic := '0';
	signal S1                 : std_logic := '0';
	signal S2                 : std_logic := '0';
	signal U1                 : std_logic := '0';
	signal U2                 : std_logic := '0';

	signal blx_comb           : std_logic := '0';
	signal W_1VF              : std_logic := '0';
	signal W_256HnX           : std_logic := '0';
	signal W_8HF              : std_logic := '0';
	signal W_DAC_A            : std_logic := '0';
	signal W_DAC_B            : std_logic := '0';
	signal W_MISSILEn         : std_logic := '0';
	signal W_SHELLn           : std_logic := '0';
	signal ZMWR               : std_logic := '0';

	signal new_sw             : std_logic_vector( 2 downto 0) := (others => '0');
	signal on_game            : std_logic_vector( 1 downto 0) := (others => '0');
	signal ROM_D              : std_logic_vector( 7 downto 0) := (others => '0');
	signal rst_count          : std_logic_vector( 3 downto 0) := (others => '0');
	signal W_9L_Q             : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_COL              : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_B                : std_logic_vector( 1 downto 0) := (others => '0');
	signal W_G                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_R                : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_SDAT_A           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_SDAT_B           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_STARS_B          : std_logic_vector( 1 downto 0) := (others => '0');
	signal W_STARS_G          : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_STARS_R          : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_VGA_B            : std_logic_vector( 1 downto 0) := (others => '0');
	signal W_VGA_G            : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_VGA_R            : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_VID              : std_logic_vector( 1 downto 0) := (others => '0');
	signal W_VIDEO_B          : std_logic_vector( 1 downto 0) := (others => '0');
	signal W_VIDEO_G          : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_VIDEO_R          : std_logic_vector( 2 downto 0) := (others => '0');
	signal W_WAV_A0           : std_logic_vector(18 downto 0) := (others => '0');
	signal W_WAV_A1           : std_logic_vector(18 downto 0) := (others => '0');
	signal W_WAV_A2           : std_logic_vector(18 downto 0) := (others => '0');
	signal W_WAV_D0           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_WAV_D1           : std_logic_vector( 7 downto 0) := (others => '0');
	signal W_WAV_D2           : std_logic_vector( 7 downto 0) := (others => '0');
   signal W_DAC              : std_logic_vector( 3 downto 0) := (others => '0');
	signal O_VGA_HSYNC		  : std_logic;
	signal O_VGA_VSYNC		  : std_logic;
	signal comp_sync_l		  : std_logic;
	signal dbl_scan        	  : std_logic;
	
	signal resetKey : std_logic;
	signal resetClk : std_logic;
	signal master_reset : std_logic;
	signal scanSW   : std_logic_vector(13 downto 0);

	signal button_in      : std_logic_vector(8 downto 0);
	signal buttons        : std_logic_vector(8 downto 0);
	signal joystick_reg   : std_logic_vector(5 downto 0);

	signal scandblctrl   : std_logic_vector(1 downto 0) := "01";	

	-- membrane Keyboard
	signal membrane_joy_s   : std_logic_vector(8 downto 0);
	signal btn_scandb_s 			: std_logic;
	
	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s				: std_logic_vector( 5 downto 0);
	signal joysel_s			: std_logic;

begin

	-- joystick multiplex
	process (WB_CLK_6M)
	begin
		if rising_edge(WB_CLK_6M) then
			joysel_s <= not joysel_s;
			
			if joysel_s = '0' then
				joy1_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			else
			   joy2_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			end if;
			
			
		end if;
	end process;
	
	joysel_o <= joysel_s;

	mc_clocks : entity work.CLOCKGEN
	port map(
		CLKIN_IN   => clock_50_i,
--		RST_IN     => not W_RESETn,
		RST_IN     => '0',
		O_CLK_36M  => W_CLK_36M,
		O_CLK_18M  => W_CLK_18M,
		O_CLK_12M  => WB_CLK_12M,
		O_CLK_06M  => WB_CLK_6M,
		O_CLK_06Mn => W_CLK_6Mn
	);

	cpu : entity work.T80as
		port map (
			RESET_n => W_CPU_RESETn,
			CLK_n   => W_CPU_CLK,
			WAIT_n  => W_CPU_WAITn,
			INT_n   => '1',
			NMI_n   => W_CPU_NMIn,
			BUSRQ_n => '1',
			M1_n    => open,
			MREQ_n  => W_CPU_MREQn,
			IORQ_n  => open,
			RD_n    => W_CPU_RDn,
			WR_n    => W_CPU_WRn,
			RFSH_n  => W_CPU_RFSHn,
			HALT_n  => open,
			BUSAK_n => open,
			A       => W_A,
			DI      => W_BDO,
			DO      => W_BDI,
			DOE     => open
		);

	mc_cpu_ram : entity work.MC_CPU_RAM
	port map (
		I_CLK  => W_CPU_RAM_CLK,
		I_ADDR => W_A(9 downto 0),
		I_D    => W_BDI,
		I_WE   => not W_CPU_WRn,
		I_OE   => not W_CPU_RAM_RDn,
		O_D    => W_CPU_RAM_DO
	);

	mc_adec : entity work.MC_ADEC
	port map(
		I_CLK_12M     => W_CLK_12M,
		I_CLK_6M      => W_CLK_6M,
		I_CPU_CLK     => W_H_CNT(0),
		I_RSTn        => W_RESETn,

		I_CPU_A       => W_A,
		I_CPU_D       => W_BDI(0),
		I_MREQn       => W_CPU_MREQn,
		I_RFSHn       => W_CPU_RFSHn,
		I_RDn         => W_CPU_RDn,
		I_WRn         => W_CPU_WRn,
		I_H_BL        => W_H_BL,
		I_V_BLn       => W_V_BLn,

		O_WAITn       => W_CPU_WAITn,
		O_NMIn        => W_CPU_NMIn,
		O_CPU_ROM_CSn => W_CPU_ROM_CSn,
		O_CPU_RAM_RDn => W_CPU_RAM_RDn,
		O_CPU_RAM_WRn => W_CPU_RAM_WRn,
		O_CPU_RAM_CSn => W_CPU_RAM_CSn,
		O_OBJ_RAM_RDn => W_OBJ_RAM_RDn,
		O_OBJ_RAM_WRn => W_OBJ_RAM_WRn,
		O_OBJ_RAM_RQn => W_OBJ_RAM_RQn,
		O_VID_RAM_RDn => W_VID_RAM_RDn,
		O_VID_RAM_WRn => W_VID_RAM_WRn,
		O_SW0_OEn     => W_SW0_OEn,
		O_SW1_OEn     => W_SW1_OEn,
		O_DIP_OEn     => W_DIP_OEn,
		O_WDR_OEn     => W_WDR_OEn,
		O_LAMP_WEn    => W_LAMP_WEn,
		O_SOUND_WEn   => W_SOUND_WEn,
		O_PITCHn      => W_PITCHn,
		O_H_FLIP      => W_H_FLIP,
		O_V_FLIP      => W_V_FLIP,
		O_BD_G        => W_BD_G,
		O_STARS_ON    => W_STARS_ON
	);

	mc_inport : entity work.MC_INPORT
	port map (
		I_COIN1    => not C1,   --  ACTIVE HI
		I_COIN2    => not C2,   --  ACTIVE HI
		I_1P_LE    => not L1,   --  ACTIVE HI
		I_1P_RI    => not R1,   --  ACTIVE HI
		I_1P_SH    => not J1,   --  ACTIVE HI
		I_2P_LE    => not L2,   --  ACTIVE HI
		I_2P_RI    => not R2,   --  ACTIVE HI
		I_2P_SH    => not J2,   --  ACTIVE HI
		I_1P_START => not S1,   --  ACTIVE HI
		I_2P_START => not S2,   --  ACTIVE HI
		I_SW0_OEn  => W_SW0_OEn,
		I_SW1_OEn  => W_SW1_OEn,
		I_DIP_OEn  => W_DIP_OEn,
		O_D        => W_SW_DO
	);

	roms : entity work.GALAXIAN_ROMS
	port map(
		I_ROM_CLK            => W_CLK_12M,
		I_ADDR(18 downto 16) => "000",
		I_ADDR(15 downto 0)  => W_A(15 downto 0),
		O_DATA               => ROM_D
	);

	mc_hv : entity work.MC_HV_COUNT
	port map(
		I_CLK    => WB_CLK_6M,
		I_RSTn   => W_RESETn,
		O_H_CNT  => W_H_CNT,
		O_H_SYNC => W_H_SYNC,
		O_H_BL   => W_H_BL,
		O_V_CNT  => W_V_CNT,
		O_V_SYNC => W_V_SYNC,
		O_V_BL2n => W_V_BL2n,
		O_V_BLn  => W_V_BLn,
		O_C_BLn  => W_C_BLn
	);

	mc_vid : entity work.MC_VIDEO
	port map(
		I_CLK_18M     => W_CLK_18M,
		I_CLK_12M     => W_CLK_12M,
		I_CLK_6M      => W_CLK_6M,
		I_CLK_6Mn     => W_CLK_6Mn,
		I_H_CNT       => W_H_CNT,
		I_V_CNT       => W_V_CNT,
		I_H_FLIP      => W_H_FLIP,
		I_V_FLIP      => W_V_FLIP,
		I_V_BLn       => W_V_BLn,
		I_C_BLn       => W_C_BLn,
		I_A           => W_A(9 downto 0),
		I_OBJ_SUB_A   => "000",
		I_BD          => W_BDI,
		I_OBJ_RAM_RQn => W_OBJ_RAM_RQn,
		I_OBJ_RAM_RDn => W_OBJ_RAM_RDn,
		I_OBJ_RAM_WRn => W_OBJ_RAM_WRn,
		I_VID_RAM_RDn => W_VID_RAM_RDn,
		I_VID_RAM_WRn => W_VID_RAM_WRn,
		O_C_BLnX      => W_C_BLnX,
		O_8HF         => W_8HF,
		O_256HnX      => W_256HnX,
		O_1VF         => W_1VF,
		O_MISSILEn    => W_MISSILEn,
		O_SHELLn      => W_SHELLn,
		O_BD          => W_VID_DO,
		O_VID         => W_VID,
		O_COL         => W_COL
	);

	mc_col_pal : entity work.MC_COL_PAL
	port map(
		I_CLK_12M    => W_CLK_12M,
		I_CLK_6M     => W_CLK_6M,
		I_VID        => W_VID,
		I_COL        => W_COL,
		I_C_BLnX     => W_C_BLnX,
		O_C_BLX      => W_C_BLX,
		O_STARS_OFFn => W_STARS_OFFn,
		O_R          => W_VIDEO_R,
		O_G          => W_VIDEO_G,
		O_B          => W_VIDEO_B
	);

	mc_stars : entity work.MC_STARS
	port map (
		I_CLK_18M    => W_CLK_18M,
		I_CLK_6M     => WB_CLK_6M,
		I_H_FLIP     => W_H_FLIP,
		I_V_SYNC     => W_V_SYNC,
		I_8HF        => W_8HF,
		I_256HnX     => W_256HnX,
		I_1VF        => W_1VF,
		I_2V         => W_V_CNT(1),
		I_STARS_ON   => W_STARS_ON,
		I_STARS_OFFn => W_STARS_OFFn,
		O_R          => W_STARS_R,
		O_G          => W_STARS_G,
		O_B          => W_STARS_B,
		O_NOISE      => open
	);

	mix : entity work.MC_VIDEO_MIX
	port map(
		I_VID_R    => W_VIDEO_R,
		I_VID_G    => W_VIDEO_G,
		I_VID_B    => W_VIDEO_B,
		I_STR_R    => W_STARS_R,
		I_STR_G    => W_STARS_G,
		I_STR_B    => W_STARS_B,
		I_C_BLnXX  => not W_C_BLX,
		I_C_BLX    => blx_comb,
		I_MISSILEn => W_MISSILEn,
		I_SHELLn   => W_SHELLn,
		O_R        => W_R,
		O_G        => W_G,
		O_B        => W_B
	);

	-- VGA scan doubler
	vga_scandbl : entity work.VGA_SCANDBL
	port map(
		-- input
		CLK     => W_CLK_6M,
		CLK_X2  => W_CLK_12M,
		I_R     => W_R,
		I_G     => W_G,
		I_B     => W_B,
		I_HSYNC => W_H_SYNC,
		I_VSYNC => W_V_SYNC,
		-- output
		O_R     => W_VGA_R,
		O_G     => W_VGA_G,
		O_B     => W_VGA_B,
		O_HSYNC => O_VGA_HSYNC,
		O_VSYNC => O_VGA_VSYNC,
      scanlines    =>  scandblctrl(1) xor scanSW(8)		
	);

	mc_sound_a : entity work.MC_SOUND_A
	port map(
		I_CLK_12M => W_CLK_12M,
		I_CLK_6M  => W_CLK_6M,
		I_H_CNT1  => W_H_CNT(1),
		I_BD      => W_BDI,
		I_PITCHn  => W_PITCHn,
		I_VOL1    => W_VOL1,
		I_VOL2    => W_VOL2,
		O_SDAT    => W_SDAT_A,
		O_DO      => open
	);

	mc_sound_b : entity work.MC_SOUND_B
	port map(
		I_CLK1        => W_CLK_6M,
		I_RSTn        => rst_count(3),
		I_SW          => new_sw,
		I_DAC         => W_DAC,
		I_FS          => W_FS,
		O_SDAT        => W_SDAT_B
	);

	wav_dac_a : entity work.dac
	port map(
		clk_i   => W_CLK_18M,
		res_n_i => W_RESETn,
		dac_i   => W_SDAT_A,
		dac_o   => W_DAC_A
	);

	wav_dac_b : entity work.dac
	port map(
		clk_i   => W_CLK_18M,
		res_n_i => W_RESETn,
		dac_i   => W_SDAT_B,
		dac_o   => W_DAC_B
	);
	
--------------------------------

  
 -- button_in(8) <= ext_ic and not scanSW(13); --ic
 -- button_in(7) <= not ext_p2 or scanSW(12);  --2p
 -- button_in(6) <= not ext_p1 or scanSW(11);  --1p
  button_in(8) <= scanSW(13) or membrane_joy_s(6); --ic
  button_in(7) <= scanSW(12) or membrane_joy_s(8); --2p
  button_in(6) <= scanSW(11) or membrane_joy_s(7); --1p
  
  button_in(5) <= joy1_s(5) and not scanSW(5) and not membrane_joy_s(5); -- fire1 / enter / z / space
  button_in(4) <= joy1_s(4) and not scanSW(4) and not membrane_joy_s(4); -- fire1 / enter / z / space
  button_in(2) <= joy1_s(3) and not scanSW(3) and not membrane_joy_s(3); -- right
  button_in(3) <= joy1_s(2) and not scanSW(2) and not membrane_joy_s(2); -- left
  button_in(0) <= joy1_s(1) and not scanSW(1) and not membrane_joy_s(1); -- down
  button_in(1) <= joy1_s(0) and not scanSW(0) and not membrane_joy_s(0); -- up
  
  --Swap directions for horizontal screen help
  buttons(0) <= button_in(0) when scanSW(9) = '0' else button_in(2);
  buttons(1) <= button_in(1) when scanSW(9) = '0' else button_in(3);
  buttons(2) <= button_in(2) when scanSW(9) = '0' else button_in(1);
  buttons(3) <= button_in(3) when scanSW(9) = '0' else button_in(0);
  buttons(8 downto 4) <= button_in(8 downto 4);  

--------- BUTTONS       ---------------------
	U1 <= not buttons(0) when HWSEL_GALAXIAN  else buttons(1);
   D1 <= not buttons(1) when HWSEL_GALAXIAN  else buttons(0);
   L1 <= not buttons(2) when HWSEL_GALAXIAN  else buttons(3);
   R1 <= not buttons(3) when HWSEL_GALAXIAN  else buttons(2);
   J1 <= not buttons(4) when HWSEL_GALAXIAN  else buttons(4);
	
	S1 <= not buttons(6);
	S2 <= not buttons(7);

	C1 <= not buttons(8); --sin el or, para coin y start separadas
	C2 <= '1';--not buttons(8);	

	U2 <= U1;
	D2 <= D1;
	L2 <= L1 when HWSEL_GALAXIAN  else U1;
	R2 <= R1 when HWSEL_GALAXIAN  else D1;
	J2 <= J1;

--  O_LED <= scanSW(9); --pad directions switch status

-------- VIDEO  -----------------------------
  dbl_scan <=  scandblctrl(0) xor scanSW(6); -- 1 = VGAS  0 = RGB

	blx_comb <= W_C_BLX or (not W_V_BL2n);

  p_comp_sync : process(W_H_SYNC, W_V_SYNC)
   begin
    comp_sync_l <= (not W_V_SYNC) and (not W_H_SYNC);
   end process;

  
  p_video_ouput : process(W_CLK_36M)
  begin
    if rising_edge(W_CLK_36M) then	
     if (dbl_scan = '1') then	 
-- con scandoubler
		hsync_o <= O_VGA_HSYNC;
		vsync_o <= O_VGA_VSYNC;

		rgb_r_o(2 downto 0) <= W_VGA_R(0) & W_VGA_R(1) & W_VGA_R(2);
		rgb_g_o(2 downto 0) <= W_VGA_G(0) & W_VGA_G(1) & W_VGA_G(2);
		rgb_b_o(2 downto 0) <= W_VGA_B(0) & W_VGA_B(1) & '0';
	  else
-- sin scandoubler
		hsync_o <= comp_sync_l;
		vsync_o <= '1';

		rgb_r_o(2 downto 0) <= W_R(0) & W_R(1) & W_R(2);
		rgb_g_o(2 downto 0) <= W_G(0) & W_G(1) & W_G(2);
		rgb_b_o(2 downto 0) <= W_B(0) & W_B(1) & '0';
    end if;
	end if;
  end process;	
  
--	O_PAL <= '1';
--	O_NTSC <= '0';

-----  CPU I/F  -------------------------------------

	W_CPU_RESETn  <= W_RESETn;
	W_CPU_CLK     <= W_H_CNT(0);
	W_CPU_RAM_CLK <= W_CLK_12M and (not W_CPU_RAM_CSn);

	W_CPU_ROM_DOB <= x"00" when W_CPU_ROM_CSn = '1' else W_CPU_ROM_DO ;

	W_CLK_12M <= WB_CLK_12M;
	W_CLK_6M  <= WB_CLK_6M;
--	W_RESETn  <= I_SW(8) or I_SW(7) or I_SW(6)     or I_SW(5);
--	W_RESETn  <= '1';
	W_RESETn  <= not resetKey and btn_reset_n_i;
	W_BDO     <= W_SW_DO  or W_VID_DO or W_CPU_RAM_DO or W_CPU_ROM_DOB ;


---------- SOUND I/F -----------------------------
	audioext_l_o <= W_DAC_A;
	audioext_r_o <= W_DAC_B;

	new_sw <= (on_game(1) and on_game(0)) & W_HIT & W_FIRE;

	process(W_H_CNT(0), W_RESETn)
	begin
		if (W_RESETn = '0') then
			rst_count <= (others => '0');
		elsif rising_edge( W_H_CNT(0)) then
			if ( rst_count /= x"f") then
				rst_count <= rst_count + 1;
			end if;
		end if;
	end process;

-----  Parts 9L ---------

	process(W_CLK_12M, W_RESETn)
	begin
		if (W_RESETn = '0') then
			W_9L_Q <= (others => '0');
		elsif rising_edge(W_CLK_12M) then
			if (W_SOUND_WEn = '0') then
				case(W_A(2 downto 0)) is
					when "000" => W_9L_Q(0) <= W_BDI(0);
					when "001" => W_9L_Q(1) <= W_BDI(0);
					when "010" => W_9L_Q(2) <= W_BDI(0);
					when "011" => W_9L_Q(3) <= W_BDI(0);
					when "100" => W_9L_Q(4) <= W_BDI(0);
					when "101" => W_9L_Q(5) <= W_BDI(0);
					when "110" => W_9L_Q(6) <= W_BDI(0);
					when "111" => W_9L_Q(7) <= W_BDI(0);
					when others => null;
				end case;
			end if;
		end if;
	end process;

	W_VOL1 <= W_9L_Q(6);
	W_VOL2 <= W_9L_Q(7);
	W_FIRE <= W_9L_Q(5);
	W_HIT  <= W_9L_Q(3);
	W_FS(2)  <= W_9L_Q(2);
	W_FS(1)  <= W_9L_Q(1);
	W_FS(0)  <= W_9L_Q(0);

-----  Parts 9M ---------
	process(W_CLK_12M, W_RESETn)
	begin
		if (W_RESETn = '0') then
			W_DAC   <= (others=>'0');
		elsif rising_edge(W_CLK_12M) then
			if (W_LAMP_WEn = '0') then
				case(W_A(2 downto 0)) is
					-- next 4 outputs go off board via ULN2075 buffer
--					when "000" => 1P START  <= W_BDI(0);
--					when "001" => 2P START  <= W_BDI(0);
--					when "010" => COIN LOCK <= W_BDI(0);
--					when "011" => COIN CTR  <= W_BDI(0);
					when "100" => W_DAC(0)  <= W_BDI(0); --   1M
					when "101" => W_DAC(1)  <= W_BDI(0); -- 470K
					when "110" => W_DAC(2)  <= W_BDI(0); -- 220K
					when "111" => W_DAC(3)  <= W_BDI(0); -- 100K
					when others => null;
				end case;
			end if;
		end if;
	end process;

------ CPU DATA WATCH -------------------------------
	ZMWR <= W_CPU_MREQn or W_CPU_WRn ;

	process(W_CPU_CLK)
	begin
		if rising_edge(W_CPU_CLK) then
			if (ZMWR = '0') then
				if (W_A = x"4007") then
					if (W_BDI = x"00") then
						on_game(0) <= '1';
					else
						on_game(0) <= '0';
					end if;
					if (W_A = x"4005") then
						if (W_BDI = x"03" or W_BDI = x"04") then
							on_game(1) <= '1';
						else
							on_game(1) <= '0';
						end if;
					end if;
				end if;
			end if;
		end if;
	end process;

--------- ROM           -------------------------------------------------------
	process(W_CLK_12M)
	begin
		if rising_edge(W_CLK_12M) then
			W_CPU_ROM_DO <= ROM_D;
		end if;
	end process;

-------------------------------------------------------------------------------

  
 --0x8FD5 SRAM (SCANDBLCTRL ZXUNO REG)  
-- sram_addr <= "000001000111111010101"; 	
-- scandblctrl <= sram_dq(1 downto 0);  
-- sram_we_n <= '1';

	
---- keyboard module
  keyboard : entity work.keyboard 
	  port map (
		CLOCK => W_CLK_18M,
		PS2_CLK => ps2_clk_io,
		PS2_DATA => ps2_data_io,
		resetKey => resetKey,
		MRESET => master_reset,
		scanSW => scanSW
	);
  
-----------------Multiboot-------------
--	multiboot : entity work.multiboot 
--	port map (
--	  clk_icap => W_CLK_18M,
--	  REBOOT => master_reset
 -- 	  --REBOOT => master_reset or not ext_rst
--	);    


	------------------------------------------------------------------
	-- membrane keyboard
	------------------------------------------------------------------

 debounce_nmi : entity work.debounce
  GENERIC map
  (
    counter_size  => 3
  )
  PORT map
  (
    clk_i     => W_CLK_18M,
    button_i  => (not btn_multiface_n_i) and membrane_joy_s(8),
    result_o  => btn_scandb_s    
	);
	
	process(btn_scandb_s)
	begin
		if rising_edge(btn_scandb_s) then
			scandblctrl(0) <= not scandblctrl(0);
		end if;
	end process;

	 membrane_joystick : entity work.membrane_joystick
	 port map(
	
      clock       		=> W_CLK_36M,
      reset       		=> not W_RESETn,

      membrane_joy_o  	=> membrane_joy_s, -- P2, P1, COIN, F2, F1, R, L, D, U
		
      keyb_row_o   		=> keyb_row_o,   
      i_membrane_cols   => keyb_col_i
  
	);


  -- TODO: add support for HDMI output
    OBUFDS_c0  : OBUFDS port map ( O  => hdmi_p_o(0), OB => hdmi_n_o(0), I => '1');
    OBUFDS_c1  : OBUFDS port map ( O  => hdmi_p_o(1), OB => hdmi_n_o(1), I => '1');
    OBUFDS_c2  : OBUFDS port map ( O  => hdmi_p_o(2), OB => hdmi_n_o(2), I => '1');
    OBUFDS_clk : OBUFDS port map ( O  => hdmi_p_o(3), OB => hdmi_n_o(3), I => '1');


end RTL;
