
module dpram 
(
    input                       clock_a,        
    input  [addr_width-1:0]     address_a,      
    input  [data_width-1:0]     data_a,         
    input                       enable_a,       
    input                       wren_a,         
    output reg [data_width-1:0] q_a,            
    input                       cs_a,           

    input                       clock_b,        
    input  [addr_width-1:0]     address_b,      
    input  [data_width-1:0]     data_b,         
    input                       enable_b,       
    input                       wren_b,         
    output reg [data_width-1:0] q_b,            
    input                       cs_b           
);

parameter addr_width = 8;
parameter data_width = 8;

integer f;
reg [(data_width-1):0] ram[0:((2**addr_width)-1)];
initial begin  
  for ( f=0; f<((2**addr_width)-1); f=f+1 ) 
  begin
    ram[f] = 8'hFF;
  end
end

// Port A
always @(posedge clock_a)
begin
    if (enable_a == 1'b1 && cs_a == 1'b1)
      if (wren_a == 1'b1)
          begin
            ram[address_a] = data_a;
            q_a = data_a;
          end
      else
        q_a = ram[address_a];
      
end 

// Port B
always @(posedge clock_b)
begin
    if (enable_b == 1'b1 && cs_b == 1'b1)
      if (wren_b == 1'b1)
          begin
            ram[address_b] = data_b;
            q_b = data_b;
          end
      else
        q_b = ram[address_b];
      
end 

endmodule