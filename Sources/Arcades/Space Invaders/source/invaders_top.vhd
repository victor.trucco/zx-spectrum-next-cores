---------------------------------------------------------------------------------
-- Space Invaders Arcade Port to ZX-UNO by Quest 2017 
---------------------------------------------------------------------------------

-- Space Invaders top level for
-- ps/2 keyboard interface with sound and scan doubler MikeJ
--
-- Version : 0300
--
-- Copyright (c) 2002 Daniel Wallner (jesus@opencores.org)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-- The latest version of this file can be found at:
--      http://www.fpgaarcade.com
--
-- Limitations :
--
-- File history :
--
--      0241 : First release
--
--      0242 : added the ROM from mw8080.vhd
--
--      0300 : MikeJ tidy up for audio release


-------------------------------------------------------------------------------
--
-- ZX Spectrum Next top by Victor Trucco - 2020
--
-------------------------------------------------------------------------------


library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;

library UNISIM;
  use UNISIM.Vcomponents.all;

entity invaders_top is
	port(
			 -- Clocks
      clock_50_i        : in    std_logic;

      -- SRAM (AS7C34096)
      ram_addr_o        : out   std_logic_vector(18 downto 0)  := (others => '0');
      ram_data_io       : inout std_logic_vector(15 downto 0)  := (others => 'Z');
      ram_oe_n_o        : out   std_logic                      := '1';
      ram_we_n_o        : out   std_logic                      := '1';
      ram_ce_n_o        : out   std_logic_vector( 3 downto 0)  := (others => '1');

      -- PS2
      ps2_clk_io        : inout std_logic                      := 'Z';
      ps2_data_io       : inout std_logic                      := 'Z';
      ps2_pin6_io       : inout std_logic                      := 'Z';  -- Mouse clock
      ps2_pin2_io       : inout std_logic                      := 'Z';  -- Mouse data

      -- SD Card
      sd_cs0_n_o        : out   std_logic                      := '1';
      sd_cs1_n_o        : out   std_logic                      := '1';
      sd_sclk_o         : out   std_logic                      := '0';
      sd_mosi_o         : out   std_logic                      := '0';
      sd_miso_i         : in    std_logic;

      -- Flash
      flash_cs_n_o      : out   std_logic                      := '1';
      flash_sclk_o      : out   std_logic                      := '0';
      flash_mosi_o      : out   std_logic                      := '0';
      flash_miso_i      : in    std_logic;
      flash_wp_o        : out   std_logic                      := '0';
      flash_hold_o      : out   std_logic                      := '1';

      -- Joystick
      joyp1_i           : in    std_logic;
      joyp2_i           : in    std_logic;
      joyp3_i           : in    std_logic;
      joyp4_i           : in    std_logic;
      joyp6_i           : in    std_logic;
      joyp7_o           : out   std_logic                      := '1';
      joyp9_i           : in    std_logic;
      joysel_o          : out   std_logic                      := '0';

      -- Audio
      audioext_l_o      : out   std_logic                      := '0';
      audioext_r_o      : out   std_logic                      := '0';
      audioint_o        : out   std_logic                      := '0';

      -- K7
      ear_port_i        : in    std_logic;
      mic_port_o        : out   std_logic                      := '0';

      -- Buttons
      btn_divmmc_n_i    : in    std_logic;
      btn_multiface_n_i : in    std_logic;
      btn_reset_n_i     : in    std_logic;

      -- Matrix keyboard
      keyb_row_o        : out   std_logic_vector( 7 downto 0)  := (others => 'Z');
      keyb_col_i        : in    std_logic_vector( 6 downto 0);

      -- Bus
      bus_rst_n_io      : inout std_logic                      := 'Z';
      bus_clk35_o       : out   std_logic                      := 'Z';
      bus_addr_o        : out   std_logic_vector(15 downto 0)  := (others => 'Z');
      bus_data_io       : inout std_logic_vector( 7 downto 0)  := (others => 'Z');
      bus_int_n_io      : inout std_logic                      := 'Z';
      bus_nmi_n_i       : in    std_logic;
      bus_ramcs_i       : in    std_logic;
      bus_romcs_i       : in    std_logic;
      bus_wait_n_i      : in    std_logic;
      bus_halt_n_o      : out   std_logic                      := 'Z';
      bus_iorq_n_o      : out   std_logic                      := 'Z';
      bus_m1_n_o        : out   std_logic                      := 'Z';
      bus_mreq_n_o      : out   std_logic                      := 'Z';
      bus_rd_n_o        : out   std_logic                      := 'Z';
      bus_wr_n_o        : out   std_logic                      := 'Z';
      bus_rfsh_n_o      : out   std_logic                      := 'Z';
      bus_busreq_n_i    : in    std_logic;
      bus_busack_n_o    : out   std_logic                      := 'Z';
      bus_iorqula_n_i   : in    std_logic;

      -- VGA
      rgb_r_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_g_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_b_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      hsync_o           : out   std_logic                      := '1';
      vsync_o           : out   std_logic                      := '1';
      csync_o           : out   std_logic                      := 'Z';

      -- HDMI
      hdmi_p_o          : out   std_logic_vector(3 downto 0);
      hdmi_n_o          : out   std_logic_vector(3 downto 0);

      -- I2C (RTC and HDMI)
      i2c_scl_io        : inout std_logic                      := 'Z';
      i2c_sda_io        : inout std_logic                      := 'Z';

      -- ESP
      esp_gpio0_io      : inout std_logic                      := 'Z';
      esp_gpio2_io      : inout std_logic                      := 'Z';
      esp_rx_i          : in    std_logic;
      esp_tx_o          : out   std_logic                      := '1';

      -- PI GPIO
      accel_io          : inout std_logic_vector(27 downto 0)  := (others => 'Z');

      -- Vacant pins
      extras_io         : inout std_logic := 'Z'
		);
end invaders_top;

architecture rtl of invaders_top is

	signal I_RESET_L       : std_logic;
	signal Clk             : std_logic;
	signal Clk_x2          : std_logic;
	signal Rst_n_s         : std_logic;

	signal DIP             : std_logic_vector(8 downto 1);
	signal RWE_n           : std_logic;
	signal Video           : std_logic;
	signal VideoRGB        : std_logic_vector(2 downto 0);
	signal VideoRGB_X2     : std_logic_vector(7 downto 0);
	signal HSync           : std_logic;
	signal VSync           : std_logic;
	signal HSync_X2        : std_logic;
	signal VSync_X2        : std_logic;

	signal AD              : std_logic_vector(15 downto 0);
	signal RAB             : std_logic_vector(12 downto 0);
	signal RDB             : std_logic_vector(7 downto 0);
	signal RWD             : std_logic_vector(7 downto 0);
	signal IB              : std_logic_vector(7 downto 0);
	signal SoundCtrl3      : std_logic_vector(5 downto 0);
	signal SoundCtrl5      : std_logic_vector(5 downto 0);

	signal Buttons         : std_logic_vector(8 downto 0);
	signal Buttons_n       : std_logic_vector(9 downto 1);

	signal Tick1us         : std_logic;

	signal Reset           : std_logic;

	signal rom_data_0      : std_logic_vector(7 downto 0);
	signal rom_data_1      : std_logic_vector(7 downto 0);
	signal rom_data_2      : std_logic_vector(7 downto 0);
	signal rom_data_3      : std_logic_vector(7 downto 0);
	signal ram_we          : std_logic;
	--
	signal HCnt            : std_logic_vector(11 downto 0);
	signal VCnt            : std_logic_vector(11 downto 0);
	signal HSync_t1        : std_logic;
	signal Overlay_G1      : boolean;
	signal Overlay_G2      : boolean;
	signal Overlay_R1      : boolean;
	signal Overlay_G1_VCnt : boolean;
  --
   signal button_in        : std_logic_vector(8 downto 0);
   signal button_debounced : std_logic_vector(8 downto 0);
	--
	signal Audio           : std_logic_vector(7 downto 0);
	signal AudioPWM        : std_logic;
	signal comp_sync_l : std_logic;
	signal dbl_scan        : std_logic;
  
	signal resetKey : std_logic;
	signal resetClk : std_logic;
	signal master_reset : std_logic;
	signal scanSW   : std_logic_vector(13 downto 0);

	signal buttonsJ        : std_logic_vector(8 downto 0);
	
	signal scandblctrl   : std_logic_vector(1 downto 0) := "01";	

	-- membrane Keyboard
	signal membrane_joy_s   : std_logic_vector(8 downto 0);
	
	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s				: std_logic_vector( 5 downto 0);
	signal joysel_s			: std_logic;
	signal btn_scandb_s		: std_logic;

begin

	-- joystick multiplex
	process (Clk)
	begin
		if rising_edge(Clk) then
			joysel_s <= not joysel_s;
			
			if joysel_s = '0' then
				joy1_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			else
			   joy2_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			end if;
			
			
		end if;
	end process;
	
	joysel_o <= joysel_s;

  I_RESET_L <= not resetKey and btn_reset_n_i;
  
  u_clocks : entity work.INVADERS_CLOCKS
	port map (
	   I_CLK_REF  => clock_50_i,
	   I_RESET_L  => '1',
	   --
	   O_CLK      => Clk,
	   O_CLK_X2   => Clk_x2
	 );

	Buttons_n <= not Buttons(8 downto 0);
	DIP <= "10000000";

	core : entity work.invaders
		port map(
			Rst_n      => I_RESET_L,
			Clk        => Clk,
			MoveLeft   => Buttons(0),
			MoveRight  => Buttons(1),
			Coin       => Buttons(2),
			Sel1Player => Buttons(3),
			Sel2Player => Buttons(5), 
			Fire       => Buttons(4),
			DIP        => DIP,
			RDB        => RDB,
			IB         => IB,
			RWD        => RWD,
			RAB        => RAB,
			AD         => AD,
			SoundCtrl3 => SoundCtrl3,
			SoundCtrl5 => SoundCtrl5,
			Rst_n_s    => Rst_n_s,
			RWE_n      => RWE_n,
			Video      => Video,
			HSync      => HSync,
			VSync      => VSync
			);
	--
	-- ROM
	--
	u_rom : entity work.INVADERS_ROM
	  port map (
		CLK         => Clk,
		ENA         => '1',
		ADDR        => AD(12 downto 0),
		DATA        => IB
		);
	--
	-- RAM
	--
	ram_we <= not RWE_n;

	rams : for i in 0 to 3 generate
	  u_ram : component RAMB16_S2
	  port map (
		do   => RDB((i*2)+1 downto (i*2)),
		addr => RAB,
		clk  => Clk,
		di   => RWD((i*2)+1 downto (i*2)),
		en   => '1',
		ssr  => '0',
		we   => ram_we
		);
	end generate;
	--
	-- Glue
	--
	process (Rst_n_s, Clk)
		variable cnt : unsigned(3 downto 0);
	begin
		if Rst_n_s = '0' then
			cnt := "0000";
			Tick1us <= '0';
		elsif Clk'event and Clk = '1' then
			Tick1us <= '0';
			if cnt = 9 then
				Tick1us <= '1';
				cnt := "0000";
			else
				cnt := cnt + 1;
			end if;
		end if;
	end process;

  
  
--  button_in(8) <= ext_ic and not scanSW(13); -- ic
--  button_in(7) <= ext_p2 and not scanSW(12); -- p2
--  button_in(6) <= ext_p1 and not scanSW(11); -- p1
  button_in(8) <= not scanSW(13) and not membrane_joy_s(6); -- ic
  button_in(7) <= not scanSW(12) and not membrane_joy_s(8); -- p2
  button_in(6) <= not scanSW(11) and not membrane_joy_s(7); -- p1

  button_in(5) <= joy1_s(5) and not scanSW(5) and not membrane_joy_s(5); --fire2 / x / lwin
  button_in(4) <= joy1_s(4) and not scanSW(4) and not membrane_joy_s(4); -- fire1 / enter / z / space
  button_in(3) <= joy1_s(3) and not scanSW(3) and not membrane_joy_s(3); -- right
  button_in(2) <= joy1_s(2) and not scanSW(2) and not membrane_joy_s(2); -- left
  button_in(1) <= joy1_s(1) and not scanSW(1) and not membrane_joy_s(1); -- down
  button_in(0) <= joy1_s(0) and not scanSW(0) and not membrane_joy_s(0); -- up
  
  --Swap directions for horizontal screen help
  buttonsJ(0) <= button_in(0) when scanSW(9) = '0' else button_in(2);
  buttonsJ(1) <= button_in(1) when scanSW(9) = '0' else button_in(3);
  buttonsJ(2) <= button_in(2) when scanSW(9) = '0' else button_in(1);
  buttonsJ(3) <= button_in(3) when scanSW(9) = '0' else button_in(0);
  buttonsJ(8 downto 4) <= button_in(8 downto 4); 

  u_debounce : entity work.BUTTON_DEBOUNCE
  generic map (
    G_WIDTH => 9
    )
  port map (
    I_BUTTON => buttonsJ, --button_in,
    O_BUTTON => button_debounced,
    CLK      => clk
    );

  p_input_registers : process
	begin
    wait until rising_edge(Clk);
	  if I_RESET_L = '0' then
			Buttons <= (others => '0');
		end if;
		if Rst_n_s = '0' then
			Buttons <= (others => '0');
		else
			Buttons(0) <= button_debounced(2); -- Left
			Buttons(1) <= button_debounced(3); -- Right
			Buttons(2) <= button_debounced(8); -- Coin 
			Buttons(3) <= button_debounced(6); -- Player1 
			Buttons(4) <= button_debounced(4); -- Fire
			Buttons(5) <= button_debounced(7); -- 2Player start
		end if;
	end process;
  --
  -- Video Output
  --
  p_overlay : process(Rst_n_s, Clk)
	variable HStart : boolean;
  begin
	if Rst_n_s = '0' then
	  HCnt <= (others => '0');
	  VCnt <= (others => '0');
	  HSync_t1 <= '0';
	  Overlay_G1_VCnt <= false;
	  Overlay_G1 <= false;
	  Overlay_G2 <= false;
	  Overlay_R1 <= false;
	elsif Clk'event and Clk = '1' then
	  HSync_t1 <= HSync;
	  HStart := (HSync_t1 = '0') and (HSync = '1');-- rising

	  if HStart then
		HCnt <= (others => '0');
	  else
		HCnt <= HCnt + "1";
	  end if;

	  if (VSync = '0') then
		VCnt <= (others => '0');
	  elsif HStart then
		VCnt <= VCnt + "1";
	  end if;

	  if HStart then
		if (Vcnt = x"1F") then
		  Overlay_G1_VCnt <= true;
		elsif (Vcnt = x"95") then
		  Overlay_G1_VCnt <= false;
		end if;
	  end if;

	  if (HCnt = x"027") and Overlay_G1_VCnt then
		Overlay_G1 <= true;
	  elsif (HCnt = x"046") then
		Overlay_G1 <= false;
	  end if;

	  if (HCnt = x"046") then
		Overlay_G2 <= true;
	  elsif (HCnt = x"0B6") then
		Overlay_G2 <= false;
	  end if;

	  if (HCnt = x"1A6") then
		Overlay_R1 <= true;
	  elsif (HCnt = x"1E6") then
		Overlay_R1 <= false;
	  end if;

	end if;
  end process;

  p_video_out_comb : process(Video, Overlay_G1, Overlay_G2, Overlay_R1)
  begin
	if (Video = '0') then
	  VideoRGB  <= "000";
	else
	  if Overlay_G1 or Overlay_G2 then
		VideoRGB  <= "010";
	  elsif Overlay_R1 then
		VideoRGB  <= "100";
	  else
		VideoRGB  <= "111";
	  end if;
	end if;
  end process;

  u_dblscan : entity work.DBLSCAN
	port map (
	  RGB_IN(7 downto 3) => "00000",
	  RGB_IN(2 downto 0) => VideoRGB,
	  HSYNC_IN           => HSync,
	  VSYNC_IN           => VSync,
	  
	  dbl_scan				=> dbl_scan,

	  RGB_OUT            => VideoRGB_X2,
	  HSYNC_OUT          => HSync_X2,
	  VSYNC_OUT          => VSync_X2,
	  --  NOTE CLOCKS MUST BE PHASE LOCKED !!
	  CLK                => Clk,
	  CLK_X2             => Clk_x2,
	  scanlines    =>  scandblctrl(1) xor scanSW(8)	  
	);
	
  dbl_scan <=  scandblctrl(0) xor scanSW(6); -- 1 = VGAS  0 = RGB	

  p_comp_sync : process(HSync, VSync)
   begin
     comp_sync_l <= (VSync) and (HSync);
   end process;

  p_video_ouput : process(clk_x2)
  begin
    if rising_edge(clk_x2) then
		if (dbl_scan = '1') then	
		----VGA
		  rgb_r_o <= VideoRGB_X2(2) & VideoRGB_X2(5) & '0';
		  rgb_g_o <= VideoRGB_X2(1) & VideoRGB_X2(4) & '0';
		  rgb_b_o <= VideoRGB_X2(0) & VideoRGB_X2(3) & '0';
		  hsync_o   <= not HSync_X2;
		  vsync_o   <= not VSync_X2;
		else
		-- RGB  
		  rgb_r_o <= VideoRGB(2) & VideoRGB(2) & '0';
		  rgb_g_o <= VideoRGB(1) & VideoRGB(1) & '0';
		  rgb_b_o <= VideoRGB(0) & VideoRGB(0) & '0';
		  hsync_o   <= comp_sync_l;
		  vsync_o   <= '1';
		end if;
	end if;
  end process;
  
 -- O_LED <= scanSW(9); --pad directions switch status
  
  --PAL <= '1';
  --NTSC <= '0';
  
  --
  -- Audio
  --
  u_audio : entity work.invaders_audio
	port map (
	  Clk => Clk,
	  S1  => SoundCtrl3,
	  S2  => SoundCtrl5,
	  Aud => Audio
	  );

  u_dac : entity work.dac
	generic map(
	  msbi_g => 7
	)
	port  map(
	  clk_i   => Clk,
	  res_n_i => Rst_n_s,
	  dac_i   => Audio,
	  dac_o   => AudioPWM
	);

  audioext_l_o <= AudioPWM;
  audioext_r_o <= AudioPWM;


 --0x8FD5 SRAM (SCANDBLCTRL ZXUNO REG)  
 ram_addr_o <= (others=>'0');
-- scandblctrl <= "01"; --sram_dq(1 downto 0);  
 ram_we_n_o <= '1';
  
---- keyboard module
  keyboard : entity work.keyboard 
	  port map (
		CLOCK => Clk_x2,
		PS2_CLK => ps2_clk_io,
		PS2_DATA => ps2_data_io,
		resetKey => resetKey,
		MRESET => master_reset,
		scanSW => scanSW
	);
  
-----------------Multiboot-------------
--	multiboot : entity work.multiboot 
--	port map (
--	  clk_icap => Clk,
--	  REBOOT => master_reset
--	  --REBOOT => master_reset or not ext_rst
--	);  
  

	------------------------------------------------------------------
	-- membrane keyboard
	------------------------------------------------------------------

 debounce_nmi : entity work.debounce
  GENERIC map
  (
    counter_size  => 3
  )
  PORT map
  (
    clk_i     => Clk_x2,
    button_i  => (not btn_multiface_n_i) and membrane_joy_s(8),
    result_o  => btn_scandb_s    
	);
	
	process(btn_scandb_s)
	begin
		if rising_edge(btn_scandb_s) then
			scandblctrl(0) <= not scandblctrl(0);
		end if;
	end process;
	
	 membrane_joystick : entity work.membrane_joystick
	 port map(
	
      clock       		=> Clk_x2,
      reset       		=> reset,

      membrane_joy_o  	=> membrane_joy_s, -- P2, P1, COIN, F2, F1, R, L, D, U
		
      keyb_row_o   		=> keyb_row_o,   
      i_membrane_cols   => keyb_col_i
  
	);

   

  -- TODO: add support for HDMI output
    OBUFDS_c0  : OBUFDS port map ( O  => hdmi_p_o(0), OB => hdmi_n_o(0), I => '1');
    OBUFDS_c1  : OBUFDS port map ( O  => hdmi_p_o(1), OB => hdmi_n_o(1), I => '1');
    OBUFDS_c2  : OBUFDS port map ( O  => hdmi_p_o(2), OB => hdmi_n_o(2), I => '1');
    OBUFDS_clk : OBUFDS port map ( O  => hdmi_p_o(3), OB => hdmi_n_o(3), I => '1');



end;
