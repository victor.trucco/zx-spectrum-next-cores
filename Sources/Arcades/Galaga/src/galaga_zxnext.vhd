---------------------------------------------------------------------------------
-- ZX-UNO Port & Top level by Quest 2017
---------------------------------------------------------------------------------
-- DE2-35 Top level for Galaga Midway by Dar (darfpga@aol.fr) (December 2016)
-- http://darfpga.blogspot.fr
-- Dip switch and other details : see galaga.vhd
---------------------------------------------------------------------------------

--------------------------------------------------------
--                                                    --
-- Ported to ZX Spectrum Next by Victor Trucco - 2020 --
--                                                    --
--------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.all;

entity galaga_zxnext is
port
(
 -- Clocks
      clock_50_i        : in    std_logic;

      -- SRAM (AS7C34096)
      ram_addr_o        : out   std_logic_vector(18 downto 0)  := (others => '0');
      ram_data_io       : inout std_logic_vector(15 downto 0)  := (others => 'Z');
      ram_oe_n_o        : out   std_logic                      := '1';
      ram_we_n_o        : out   std_logic                      := '1';
      ram_ce_n_o        : out   std_logic_vector( 3 downto 0)  := (others => '1');

      -- PS2
      ps2_clk_io        : inout std_logic                      := 'Z';
      ps2_data_io       : inout std_logic                      := 'Z';
      ps2_pin6_io       : inout std_logic                      := 'Z';  -- Mouse clock
      ps2_pin2_io       : inout std_logic                      := 'Z';  -- Mouse data

      -- SD Card
      sd_cs0_n_o        : out   std_logic                      := '1';
      sd_cs1_n_o        : out   std_logic                      := '1';
      sd_sclk_o         : out   std_logic                      := '0';
      sd_mosi_o         : out   std_logic                      := '0';
      sd_miso_i         : in    std_logic;

      -- Flash
      flash_cs_n_o      : out   std_logic                      := '1';
      flash_sclk_o      : out   std_logic                      := '0';
      flash_mosi_o      : out   std_logic                      := '0';
      flash_miso_i      : in    std_logic;
      flash_wp_o        : out   std_logic                      := '0';
      flash_hold_o      : out   std_logic                      := '1';

      -- Joystick
      joyp1_i           : in    std_logic;
      joyp2_i           : in    std_logic;
      joyp3_i           : in    std_logic;
      joyp4_i           : in    std_logic;
      joyp6_i           : in    std_logic;
      joyp7_o           : out   std_logic                      := '1';
      joyp9_i           : in    std_logic;
      joysel_o          : out   std_logic                      := '0';

      -- Audio
      audioext_l_o      : out   std_logic                      := '0';
      audioext_r_o      : out   std_logic                      := '0';
      audioint_o        : out   std_logic                      := '0';

      -- K7
      ear_port_i        : in    std_logic;
      mic_port_o        : out   std_logic                      := '0';

      -- Buttons
      btn_divmmc_n_i    : in    std_logic;
      btn_multiface_n_i : in    std_logic;
      btn_reset_n_i     : in    std_logic;

      -- Matrix keyboard
      keyb_row_o        : out   std_logic_vector( 7 downto 0)  := (others => 'Z');
      keyb_col_i        : in    std_logic_vector( 6 downto 0);

      -- Bus
      bus_rst_n_io      : inout std_logic                      := 'Z';
      bus_clk35_o       : out   std_logic                      := 'Z';
      bus_addr_o        : out   std_logic_vector(15 downto 0)  := (others => 'Z');
      bus_data_io       : inout std_logic_vector( 7 downto 0)  := (others => 'Z');
      bus_int_n_io      : inout std_logic                      := 'Z';
      bus_nmi_n_i       : in    std_logic;
      bus_ramcs_i       : in    std_logic;
      bus_romcs_i       : in    std_logic;
      bus_wait_n_i      : in    std_logic;
      bus_halt_n_o      : out   std_logic                      := 'Z';
      bus_iorq_n_o      : out   std_logic                      := 'Z';
      bus_m1_n_o        : out   std_logic                      := 'Z';
      bus_mreq_n_o      : out   std_logic                      := 'Z';
      bus_rd_n_o        : out   std_logic                      := 'Z';
      bus_wr_n_o        : out   std_logic                      := 'Z';
      bus_rfsh_n_o      : out   std_logic                      := 'Z';
      bus_busreq_n_i    : in    std_logic;
      bus_busack_n_o    : out   std_logic                      := 'Z';
      bus_iorqula_n_i   : in    std_logic;

      -- VGA
      rgb_r_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_g_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      rgb_b_o           : out   std_logic_vector( 2 downto 0)  := (others => '0');
      hsync_o           : out   std_logic                      := '1';
      vsync_o           : out   std_logic                      := '1';
      csync_o           : out   std_logic                      := 'Z';

      -- HDMI
      hdmi_p_o          : out   std_logic_vector(3 downto 0);
      hdmi_n_o          : out   std_logic_vector(3 downto 0);

      -- I2C (RTC and HDMI)
      i2c_scl_io        : inout std_logic                      := 'Z';
      i2c_sda_io        : inout std_logic                      := 'Z';

      -- ESP
      esp_gpio0_io      : inout std_logic                      := 'Z';
      esp_gpio2_io      : inout std_logic                      := 'Z';
      esp_rx_i          : in    std_logic;
      esp_tx_o          : out   std_logic                      := '1';

      -- PI GPIO
      accel_io          : inout std_logic_vector(27 downto 0)  := (others => 'Z');

      -- Vacant pins
      extras_io         : inout std_logic := 'Z'
	
);
end;

architecture struct of galaga_zxnext is

	signal clk        : std_logic;
	signal clk18      : std_logic;
	signal clk11      : std_logic;
	signal reset      : std_logic;
	signal pll_locked : std_logic;

	signal audio      : std_logic_vector(9 downto 0);
	signal audio_out	: std_logic;
	signal video_r		: std_logic_vector(2 downto 0);
	signal video_g		: std_logic_vector(2 downto 0);
	signal video_b		: std_logic_vector(2 downto 0);
	signal r				: std_logic_vector(2 downto 0);
	signal g				: std_logic_vector(2 downto 0);
	signal b				: std_logic_vector(1 downto 0);	
	signal vsync		: std_logic;
	signal hsync		: std_logic;
	signal csync		: std_logic;
	signal blankn     : std_logic;

	signal scanlines   : std_logic_vector(1 downto 0);
	signal comp_sync_l : std_logic;
	signal video_r_x2  : std_logic_vector(3 downto 0);
	signal video_g_x2  : std_logic_vector(3 downto 0);
	signal video_b_x2  : std_logic_vector(3 downto 0);
	signal hsync_x2    : std_logic;
	signal vsync_x2    : std_logic;  

	signal resetKey	  : std_logic;
	signal master_reset : std_logic;
	signal scanSW		  : std_logic_vector(13 downto 0);

	signal buttons      : std_logic_vector(8 downto 0);
	signal button_in    : std_logic_vector(8 downto 0);
	signal joystick_reg : std_logic_vector(5 downto 0);

	signal dbl_scan	  : std_logic; 
	signal en_vid	  : std_logic; 
  
	signal scandblctrl   : std_logic_vector(1 downto 0) := "01";	

	-- membrane Keyboard
	signal membrane_joy_s   : std_logic_vector(8 downto 0);
	

	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s				: std_logic_vector( 5 downto 0);
	signal joysel_s			: std_logic;
	signal btn_scandb_s		: std_logic;
	
begin

	-- joystick multiplex
	process (clk18)
	begin
		if rising_edge(clk18) then
			joysel_s <= not joysel_s;
			
			if joysel_s = '0' then
				joy1_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			else
			   joy2_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			end if;
			
			
		end if;
	end process;
	
	joysel_o <= joysel_s;
 
	relojes: entity work.relojes
	port map(
      CLK_IN1  => clock_50_i, 
		CLK_OUT1 => clk11,
		CLK_OUT2 => clk18,
      LOCKED	=> pll_locked
	);
	
--	NTSC <= '0';
--	PAL <= '1';

  --
  -- Audio
  --
	dac : entity work.pwm_sddac
	port  map(
		clk_i	=> clk11, --clk60,
		reset	=> reset,
		dac_i	=> audio,
		dac_o	=> audio_out,
		we		=> '1'
	);
	
	audioext_l_o <= audio_out;
	audioext_r_o <= audio_out;

	reset <= not pll_locked or resetKey or not btn_reset_n_i; 
	
	galaga : entity work.galaga
	port map(
		 clock_18     => clk18,
		 reset        => reset,
		-- tv15Khz_mode => tv15Khz_mode,
		 video_r      => r,
		 video_g      => g,
		 video_b      => b,
		 video_csync  => csync,
		 video_blankn => blankn,
		 video_hs     => hsync,
		 video_vs     => vsync,
		 ena_vidext   => en_vid,
		 audio        => audio,
		 
		 b_test       => '0',
		 b_svce       => '0', 
		 coin         => not buttons(8),
		 start1       => not buttons(6),
		 left1        => not buttons(3),
		 right1       => not buttons(2),
		 fire1        => not buttons(4),
		 start2       => not buttons(7),
		 left2        => not buttons(3),
		 right2       => not buttons(2),
		 fire2        => not buttons(4)
	);	

  
  dbl_scan <=  scandblctrl(0) xor scanSW(6); -- 1 = VGA  0 = RGB
  
  ----genera sincro compuesta
  comp_sync_l <= not (vsync xor hsync);

  p_video_ouput : process
  begin
    wait until rising_edge(clk18);
		 if (dbl_scan = '0') then  
		  rgb_r_o <= video_r;
		  rgb_g_o <= video_g;
		  rgb_b_o <= video_b;
		  
		  hsync_o <= comp_sync_l;
		  vsync_o <= '1';
		else  
		  rgb_r_o <= video_r_x2(3 downto 1);
		  rgb_g_o <= video_g_x2(3 downto 1);
		  rgb_b_o <= video_b_x2(3 downto 1);
		  
		  hsync_o <= not hsync_x2;
		  vsync_o <= not vsync_x2;
		end if;
	end process;
	
  video_r <= r when blankn = '1' else "000";
  video_g <= g when blankn = '1' else "000";
  video_b <= b & '0' when blankn = '1' else "000";	
  
  vga: entity work.scandoubler
	port map(
		clk_sys => clk11,
		scanlines => scandblctrl(1) xor scanSW(8),
		r_in   => video_r & '0',
		g_in   => video_g & '0',
		b_in   => video_b & '0',
		hs_in  => hsync,
		vs_in  => vsync,
		r_out  => video_r_x2(3 downto 1),
		g_out  => video_g_x2(3 downto 1),
		b_out  => video_b_x2(3 downto 1),
		hs_out => hsync_x2,
		vs_out => vsync_x2,
		en_vid => en_vid
	);
  
  u_debounce : entity work.BUTTON_DEBOUNCE
  generic map (
    G_WIDTH => 6
    )
  port map (
    I_BUTTON => joy1_s, --button_in,
    O_BUTTON => joystick_reg,
    CLK      => clk11
    );  

	--  button_in(8) <= not scanSW(7);
--	button_in(7) <= not scanSW(7); --pause
--	button_in(5) <= not scanSW(5); -- fire2 / x / lwin
--	button_in(5) <= joystick_reg(5) and not scanSW(5); -- fire2 / x / lwin
--	button_in(6) <= joystick_reg(4) and not scanSW(4) and not membrane_joy_s(4); -- fire1 / enter / z / space
  button_in(8) <= not scanSW(13) and not membrane_joy_s(6); --ic
  button_in(7) <= not scanSW(12) and not membrane_joy_s(8); --2p
  button_in(6) <= not scanSW(11) and not membrane_joy_s(7); --1p
	

	button_in(4) <= joystick_reg(4) and not scanSW(4) and not membrane_joy_s(4); -- fire1 / enter / z / space
	button_in(2) <= joystick_reg(3) and not scanSW(3) and not membrane_joy_s(3); -- right
	button_in(3) <= joystick_reg(2) and not scanSW(2) and not membrane_joy_s(2); -- left
	button_in(0) <= joystick_reg(1) and not scanSW(1) and not membrane_joy_s(1); -- down
	button_in(1) <= joystick_reg(0) and not scanSW(0) and not membrane_joy_s(0); -- up

	--Swap directions for horizontal screen help
	buttons(0) <= button_in(0) when scanSW(9) = '0' else button_in(2);
	buttons(1) <= button_in(1) when scanSW(9) = '0' else button_in(3);
	buttons(2) <= button_in(2) when scanSW(9) = '0' else button_in(1);
	buttons(3) <= button_in(3) when scanSW(9) = '0' else button_in(0);
	buttons(8 downto 4) <= button_in(8 downto 4); 

--	LED <= scanSW(9); --pad directions switch status  
  
	 ram_addr_o <= (others=>'0');
--	 scandblctrl <= "01"; --sram_dq(1 downto 0);  
	 ram_we_n_o <= '1';
  
	---- keyboard module
	keyboard : entity work.keyboard 
	port map (
		CLOCK 	=> clk18, --clk,
		PS2_CLK	=> ps2_clk_io,
		PS2_DATA => ps2_data_io,
		resetKey => resetKey,
		MRESET	=> master_reset,
		scanSW	=> scanSW
	);
  
-----------------Multiboot-------------
--	multiboot : entity work.multiboot 
--	port map (
--	  clk_icap	=> clk18, --clk,
--	  REBOOT		=> master_reset
--	);  	


	------------------------------------------------------------------
	-- membrane keyboard
	------------------------------------------------------------------

 debounce_nmi : entity work.debounce
  GENERIC map
  (
    counter_size  => 3
  )
  PORT map
  (
    clk_i     => clk18,
    button_i  => (not btn_multiface_n_i) and membrane_joy_s(8),
    result_o  => btn_scandb_s    
	);
	
	process(btn_scandb_s)
	begin
		if rising_edge(btn_scandb_s) then
			scandblctrl(0) <= not scandblctrl(0);
		end if;
	end process;
	
	 membrane_joystick : entity work.membrane_joystick
	 port map(
	
      clock       		=> clk18,
      reset       		=> reset,

      membrane_joy_o  	=> membrane_joy_s, -- P2, P1, COIN, F2, F1, R, L, D, U
		
      keyb_row_o   		=> keyb_row_o,   
      i_membrane_cols   => keyb_col_i
  
	);

   


  -- TODO: add support for HDMI output
    OBUFDS_c0  : OBUFDS port map ( O  => hdmi_p_o(0), OB => hdmi_n_o(0), I => '1');
    OBUFDS_c1  : OBUFDS port map ( O  => hdmi_p_o(1), OB => hdmi_n_o(1), I => '1');
    OBUFDS_c2  : OBUFDS port map ( O  => hdmi_p_o(2), OB => hdmi_n_o(2), I => '1');
    OBUFDS_clk : OBUFDS port map ( O  => hdmi_p_o(3), OB => hdmi_n_o(3), I => '1');


	
	
end struct;
