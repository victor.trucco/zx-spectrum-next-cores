------------------------------------------------------------------------------
-- Membrane Keyboard by Victor Trucco - 2020
------------------------------------------------------------------------------


library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;
  use ieee.numeric_std.all;


entity membrane_keyboard is
	port(
	
      clock       		: in  std_logic;
      reset       		: in  std_logic;

		Key_dec_i     		: in  std_logic_vector(6 downto 1);
		key_enc_o     		: out std_logic_vector(14 downto 7);
		osd_o					: out std_logic_vector(4 downto 0);
		reset_key_n_o 		: out std_logic;
				
      keyb_row_o   		: out std_logic_vector(7 downto 0);   -- physical membrane rows  0 = active, 1 = Z
      i_membrane_cols   : in  std_logic_vector(6 downto 0)    -- physical membrane cols returned, 6:5 are extra columns
		
		
  
	);
end;

architecture RTL of membrane_keyboard is
	
	signal membrane_en		: std_logic;
	signal row_en				: std_logic;
   signal key_row_s        : std_logic_vector(7 downto 0);
	signal membrane_col_s   : std_logic_vector(4 downto 0);
	signal membrane_rows    : std_logic_vector(7 downto 0);
	signal state 				: std_logic_vector(2 downto 0) := "000";
   signal clk_div          : std_logic_vector(17 downto 0) := (others => '0');
	
	type key_matrix is array (5 downto 0) of std_logic_vector(7 downto 0);
	signal keys    :   key_matrix;
	signal CS  		:   std_logic;
	signal SS  		:   std_logic;
	signal btCS  	:   std_logic;
	signal btSS  	:   std_logic;	
begin

	
   process (clock)
   begin
      if rising_edge(clock) then
         clk_div <= clk_div + 1;
      end if;
   end process;
	
	membrane_en <= '1' when clk_div(8 downto 0) = ('1' & X"FF") else '0';       
   row_en 		<= '1' when clk_div(17 downto 0) = ("11" & X"FFFF") else '0';   


   process (row_en)
   begin
      if rising_edge(row_en) then
          state <= state + 1;

			 case state is
                  when "000" =>  key_row_s <= "11111110";		
                  when "001" =>  key_row_s <= "11111101";		
                  when "010" =>  key_row_s <= "11111011";		
                  when "011" =>  key_row_s <= "11110111";		
                  when "100" =>  key_row_s <= "11101111";		
                  when "101" =>  key_row_s <= "11011111";	
                  when "110" =>  key_row_s <= "10111111";	
                  when "111" =>  key_row_s <= "01111111";	
						when others => key_row_s <= "11111111";	
				end case;		
		end if;
   end process;
   	
   membrane_mod : entity work.membrane
   port map
   (
      i_CLK             => clock,
      i_CLK_EN          => membrane_en,
      
      i_reset           => reset,
      
      i_rows            => key_row_s, 			-- which row we are testing ('0' to test)
      o_cols            => membrane_col_s, 	-- resulting col 

		-- phisical pins
      o_membrane_rows   => membrane_rows,   
      i_membrane_cols   => i_membrane_cols
   );
   
   keyb_row_o(0) <= '0' when membrane_rows(0) = '0' else 'Z';
   keyb_row_o(1) <= '0' when membrane_rows(1) = '0' else 'Z';
   keyb_row_o(2) <= '0' when membrane_rows(2) = '0' else 'Z';
   keyb_row_o(3) <= '0' when membrane_rows(3) = '0' else 'Z';
   keyb_row_o(4) <= '0' when membrane_rows(4) = '0' else 'Z';
   keyb_row_o(5) <= '0' when membrane_rows(5) = '0' else 'Z';
   keyb_row_o(6) <= '0' when membrane_rows(6) = '0' else 'Z';
   keyb_row_o(7) <= '0' when membrane_rows(7) = '0' else 'Z';
	
	-----------------------------------------------------
	-- Output	
	-----------------------------------------------------

	process(Key_dec_i, keys)
	begin
	
		if Key_dec_i(1) = '0' then -- 0 1 2 3 4 5 6 7
			key_enc_o <= keys(0); 
		end if;
		
		if Key_dec_i(2) = '0' then --8 9 () () space ? L P
			key_enc_o <= keys(1); 
		end if;
		
		if Key_dec_i(3) = '0' then -- + W E R T U I O
			key_enc_o <= keys(2);
		end if;

		if Key_dec_i(4) = '0' then -- Q D S F G H J K
			key_enc_o <= keys(3);
		end if;
		
		if Key_dec_i(5) = '0' then -- A Z X C V B M .
			key_enc_o <= keys(4);
		end if;

		if Key_dec_i(6) = '0' then -- - * / = Y(YES) N(NO) CLR ENT
			key_enc_o <= keys(5);
		end if;
		
	end process;
	
	process (clock)
   begin
	if reset = '1' then
			
			keys(0) <= (others => '1');
			keys(1) <= (others => '1');
			keys(2) <= (others => '1');
			keys(3) <= (others => '1');
			keys(4) <= (others => '1');
			keys(5) <= (others => '1');
			osd_o   <= (others => '0');
		   reset_key_n_o <= '1';
   elsif rising_edge(clock) then
	
			if key_row_s = "11111110" then -- (A8)
			
				CS     <= not membrane_col_s(0); -- Caps Shift
				keys(4)(1) <= membrane_col_s(1); -- Z
				keys(4)(2) <= membrane_col_s(2); -- X
				
				if membrane_col_s(3) = '0' and SS = '1' then -- SS + C = ?
					keys(1)(5) <= '0'; -- ? pressed
				else
					keys(1)(5) <= '1'; -- ? not pressed
					keys(4)(3) <= membrane_col_s(3); -- C
				end if;

				if membrane_col_s(4) = '0' and SS = '1' then -- SS + V = /
					keys(5)(2) <= '0'; -- / pressed
				else
					keys(5)(2) <= '1'; -- / not pressed
					keys(4)(4) <= membrane_col_s(4); -- V	
				end if;
				
			end if;
			
				
			if key_row_s = "11111101" then -- (A9)			
				keys(4)(0) <= membrane_col_s(0); -- A
				keys(3)(1) <= membrane_col_s(1); -- S
				keys(3)(2) <= membrane_col_s(2); -- D
				keys(3)(3) <= membrane_col_s(3); -- F
				keys(3)(4) <= membrane_col_s(4); -- G				
			end if;
			
			
			if key_row_s = "11111011" then -- (A10)
				keys(3)(0) <= membrane_col_s(0); -- Q
				keys(2)(1) <= membrane_col_s(1); -- W
				keys(2)(2) <= membrane_col_s(2); -- E
				keys(2)(3) <= membrane_col_s(3); -- R
				keys(2)(4) <= membrane_col_s(4); -- T	
			end if;
			
			if key_row_s = "11110111" then -- (A11)
				keys(0)(1) <= membrane_col_s(0); -- 1
				keys(0)(2) <= membrane_col_s(1); -- 2
				keys(0)(3) <= membrane_col_s(2); -- 3
				keys(0)(4) <= membrane_col_s(3); -- 4
				keys(0)(5) <= membrane_col_s(4); -- 5	
				
				if membrane_col_s(4) = '0' then osd_o(1) <= '1'; else osd_o(1) <= '0'; end if; -- 5 = left
			end if;
			
			if key_row_s = "11101111" then -- (A12)

				keys(1)(1) <= membrane_col_s(1); -- 9
				keys(1)(0) <= membrane_col_s(2); -- 8
				keys(0)(7) <= membrane_col_s(3); -- 7
				keys(0)(6) <= membrane_col_s(4); -- 6	
				
				if membrane_col_s(2) = '0' then	osd_o(0) <= '1'; else osd_o(0) <= '0'; end if; -- 8 = right
				osd_o(3) <= not membrane_col_s(3); -- 7 = up
				osd_o(2) <= not membrane_col_s(4); -- 6 = down
				
				if membrane_col_s(0) = '0' and CS = '1' then -- caps + 0 = Delete
					keys(5)(6) <= '0'; -- delete pressed
				else
					keys(5)(6) <= '1'; -- delete not pressed
					keys(0)(0) <= membrane_col_s(0); -- 0
				end if; 
			end if;	
			
			if key_row_s = "11011111" then -- (A13)
				keys(1)(7) <= membrane_col_s(0); -- P
				keys(2)(7) <= membrane_col_s(1); -- O
				keys(2)(6) <= membrane_col_s(2); -- I
				keys(2)(5) <= membrane_col_s(3); -- U
				keys(5)(4) <= membrane_col_s(4); -- Y	
			end if;	
			
			if key_row_s = "10111111" then -- (A14)
				keys(5)(7) <= membrane_col_s(0); -- Enter

				keys(3)(5) <= membrane_col_s(4); -- H	

				if membrane_col_s(0) = '0' then	osd_o(4) <= '1'; else osd_o(4) <= '0'; end if; -- Enter
				
				if membrane_col_s(3) = '0' and SS = '1' then -- SS + J = -
					keys(5)(0) <= '0'; -- - pressed
				else
					keys(5)(0) <= '1'; -- - not pressed
					keys(3)(6) <= membrane_col_s(3); -- J
				end if;
				
				if membrane_col_s(2) = '0' and SS = '1' then -- SS + K = +
					keys(2)(0) <= '0'; -- + pressed
				else
					keys(2)(0) <= '1'; -- + not pressed
					keys(3)(7) <= membrane_col_s(2); -- K
				end if;
				
				if membrane_col_s(1) = '0' and SS = '1' then -- SS + L = =
					keys(5)(3) <= '0'; -- = pressed
				else
					keys(5)(3) <= '1'; -- = not pressed
					keys(1)(6) <= membrane_col_s(1); -- L
				end if;
			end if;	
						
			if key_row_s = "01111111" then -- (A15)
				
				SS 	 <= not membrane_col_s(1); -- Symbol Shift

				keys(5)(5) <= membrane_col_s(3); -- N
				
				
				if membrane_col_s(0) = '0' and CS = '1' then -- Break Key = RESET on Odyssey
					reset_key_n_o <= '0'; 
				else --just the space key
					reset_key_n_o <= '1'; 
					keys(1)(4) <= membrane_col_s(0); -- Space
				end if; 
				
				if membrane_col_s(4) = '0' and SS = '1' then -- SS + B = *
					keys(5)(1) <= '0'; -- * pressed
				else
					keys(5)(1) <= '1'; -- * not pressed
					keys(4)(5) <= membrane_col_s(4); -- B	
				end if;
				
				if membrane_col_s(2) = '0' and SS = '1' then -- SS + M = .
					keys(4)(7) <= '0'; -- . pressed
				else
					keys(4)(7) <= '1'; -- . not pressed
					keys(4)(6) <= membrane_col_s(2); -- M
				end if;
				
			end if;	
			
		end if;
	end process;


	
end RTL;
