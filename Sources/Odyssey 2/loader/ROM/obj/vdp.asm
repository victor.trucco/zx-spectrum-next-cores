;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW32)
;--------------------------------------------------------
	.module vdp
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _vpdv
	.globl _vpdf
	.globl _vpdc
	.globl _caddr
	.globl _vaddr
	.globl _faddr
	.globl _bg
	.globl _fg
	.globl __bright
	.globl __flash
	.globl _cy
	.globl _cx
	.globl _font
	.globl _vdp_init
	.globl _vdp_setcolor
	.globl _vdp_setborder
	.globl _vdp_setflash
	.globl _vdp_setfg
	.globl _vdp_setbg
	.globl _vdp_cls
	.globl _vdp_gotoxy
	.globl _vdp_gotox
	.globl _vdp_putchar
	.globl _vdp_prints
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_SD_CONTROL	=	0x00e7
_SD_STATUS	=	0x00e7
_SD_DATA	=	0x00eb
_ULAPORT	=	0x00fe
_REG_NUM	=	0x243b
_REG_VAL	=	0x253b
_JOYPORT	=	0x263b
_HROW0	=	0xfefe
_HROW1	=	0xfdfe
_HROW2	=	0xfbfe
_HROW3	=	0xf7fe
_HROW4	=	0xeffe
_HROW5	=	0xdffe
_HROW6	=	0xbffe
_HROW7	=	0x7ffe
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_cx::
	.ds 1
_cy::
	.ds 1
__flash::
	.ds 1
__bright::
	.ds 1
_fg::
	.ds 1
_bg::
	.ds 1
_faddr::
	.ds 2
_vaddr::
	.ds 2
_caddr::
	.ds 2
_vpdc::
	.ds 1
_vpdf::
	.ds 1
_vpdv::
	.ds 1
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/vdp.c:37: void vdp_init()
;	---------------------------------
; Function vdp_init
; ---------------------------------
_vdp_init::
;src/vdp.c:42: cx = cy = 0;
	ld	hl,#_cy + 0
	ld	(hl), #0x00
	ld	hl,#_cx + 0
	ld	(hl), #0x00
;src/vdp.c:43: fg = COLOR_GRAY;
	ld	hl,#_fg + 0
	ld	(hl), #0x07
;src/vdp.c:44: bg = COLOR_BLACK;
	ld	hl,#_bg + 0
	ld	(hl), #0x00
;src/vdp.c:45: _flash = 0;
	ld	hl,#__flash + 0
	ld	(hl), #0x00
;src/vdp.c:46: _bright = 0;
	ld	hl,#__bright + 0
	ld	(hl), #0x00
;src/vdp.c:47: ULAPORT = COLOR_BLUE;
	ld	a,#0x01
	out	(_ULAPORT),a
;src/vdp.c:49: for (c = PIX_BASE; c < (PIX_BASE+6144); c++) {
	ld	bc,#0x4000
00103$:
;src/vdp.c:50: poke(c, 0);
	ld	e, c
	ld	d, b
	xor	a, a
	ld	(de),a
;src/vdp.c:49: for (c = PIX_BASE; c < (PIX_BASE+6144); c++) {
	inc	bc
	ld	a,b
	sub	a, #0x58
	jr	C,00103$
;src/vdp.c:52: for (c = CT_BASE; c < (CT_BASE+768); c++) {
	ld	bc,#0x5800
00105$:
;src/vdp.c:53: poke(c, v);
	ld	l, c
	ld	h, b
	ld	(hl),#0x07
;src/vdp.c:52: for (c = CT_BASE; c < (CT_BASE+768); c++) {
	inc	bc
	ld	a,b
	sub	a, #0x5b
	jr	C,00105$
	ret
_font:
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x24	; 36
	.db #0x24	; 36
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x24	; 36
	.db #0x7e	; 126
	.db #0x24	; 36
	.db #0x24	; 36
	.db #0x7e	; 126
	.db #0x24	; 36
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x3e	; 62
	.db #0x28	; 40
	.db #0x3e	; 62
	.db #0x0a	; 10
	.db #0x3e	; 62
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x62	; 98	'b'
	.db #0x64	; 100	'd'
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x26	; 38
	.db #0x46	; 70	'F'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x28	; 40
	.db #0x10	; 16
	.db #0x2a	; 42
	.db #0x44	; 68	'D'
	.db #0x3a	; 58
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x04	; 4
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x20	; 32
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x14	; 20
	.db #0x08	; 8
	.db #0x3e	; 62
	.db #0x08	; 8
	.db #0x14	; 20
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x3e	; 62
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3e	; 62
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x18	; 24
	.db #0x18	; 24
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x02	; 2
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x46	; 70	'F'
	.db #0x4a	; 74	'J'
	.db #0x52	; 82	'R'
	.db #0x62	; 98	'b'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x18	; 24
	.db #0x28	; 40
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x3e	; 62
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x02	; 2
	.db #0x3c	; 60
	.db #0x40	; 64
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x0c	; 12
	.db #0x02	; 2
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x18	; 24
	.db #0x28	; 40
	.db #0x48	; 72	'H'
	.db #0x7e	; 126
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7e	; 126
	.db #0x40	; 64
	.db #0x7c	; 124
	.db #0x02	; 2
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x40	; 64
	.db #0x7c	; 124
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7e	; 126
	.db #0x02	; 2
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3e	; 62
	.db #0x02	; 2
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x08	; 8
	.db #0x04	; 4
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3e	; 62
	.db #0x00	; 0
	.db #0x3e	; 62
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x20	; 32
	.db #0x10	; 16
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x4a	; 74	'J'
	.db #0x56	; 86	'V'
	.db #0x5e	; 94
	.db #0x40	; 64
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x7e	; 126
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7c	; 124
	.db #0x42	; 66	'B'
	.db #0x7c	; 124
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x7c	; 124
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x78	; 120	'x'
	.db #0x44	; 68	'D'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x44	; 68	'D'
	.db #0x78	; 120	'x'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7e	; 126
	.db #0x40	; 64
	.db #0x7c	; 124
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7e	; 126
	.db #0x40	; 64
	.db #0x7c	; 124
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x40	; 64
	.db #0x4e	; 78	'N'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x7e	; 126
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3e	; 62
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x3e	; 62
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x02	; 2
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x48	; 72	'H'
	.db #0x70	; 112	'p'
	.db #0x48	; 72	'H'
	.db #0x44	; 68	'D'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x66	; 102	'f'
	.db #0x5a	; 90	'Z'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x62	; 98	'b'
	.db #0x52	; 82	'R'
	.db #0x4a	; 74	'J'
	.db #0x46	; 70	'F'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7c	; 124
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x7c	; 124
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x52	; 82	'R'
	.db #0x4a	; 74	'J'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7c	; 124
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x7c	; 124
	.db #0x44	; 68	'D'
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x40	; 64
	.db #0x3c	; 60
	.db #0x02	; 2
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0xfe	; 254
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x24	; 36
	.db #0x18	; 24
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x42	; 66	'B'
	.db #0x5a	; 90	'Z'
	.db #0x24	; 36
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x42	; 66	'B'
	.db #0x24	; 36
	.db #0x18	; 24
	.db #0x18	; 24
	.db #0x24	; 36
	.db #0x42	; 66	'B'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x82	; 130
	.db #0x44	; 68	'D'
	.db #0x28	; 40
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7e	; 126
	.db #0x04	; 4
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x0e	; 14
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x0e	; 14
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x40	; 64
	.db #0x20	; 32
	.db #0x10	; 16
	.db #0x08	; 8
	.db #0x04	; 4
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x70	; 112	'p'
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x70	; 112	'p'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x38	; 56	'8'
	.db #0x54	; 84	'T'
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0xff	; 255
	.db #0x00	; 0
	.db #0x1c	; 28
	.db #0x22	; 34
	.db #0x78	; 120	'x'
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x7e	; 126
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x38	; 56	'8'
	.db #0x04	; 4
	.db #0x3c	; 60
	.db #0x44	; 68	'D'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x3c	; 60
	.db #0x22	; 34
	.db #0x22	; 34
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x1c	; 28
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x1c	; 28
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x3c	; 60
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x38	; 56	'8'
	.db #0x44	; 68	'D'
	.db #0x78	; 120	'x'
	.db #0x40	; 64
	.db #0x3c	; 60
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x0c	; 12
	.db #0x10	; 16
	.db #0x18	; 24
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x3c	; 60
	.db #0x04	; 4
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x78	; 120	'x'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x30	; 48	'0'
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x04	; 4
	.db #0x00	; 0
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x04	; 4
	.db #0x24	; 36
	.db #0x18	; 24
	.db #0x00	; 0
	.db #0x20	; 32
	.db #0x28	; 40
	.db #0x30	; 48	'0'
	.db #0x30	; 48	'0'
	.db #0x28	; 40
	.db #0x24	; 36
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x0c	; 12
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x68	; 104	'h'
	.db #0x54	; 84	'T'
	.db #0x54	; 84	'T'
	.db #0x54	; 84	'T'
	.db #0x54	; 84	'T'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x78	; 120	'x'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x38	; 56	'8'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x78	; 120	'x'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x78	; 120	'x'
	.db #0x40	; 64
	.db #0x40	; 64
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x3c	; 60
	.db #0x04	; 4
	.db #0x06	; 6
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x1c	; 28
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x20	; 32
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x38	; 56	'8'
	.db #0x40	; 64
	.db #0x38	; 56	'8'
	.db #0x04	; 4
	.db #0x78	; 120	'x'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x10	; 16
	.db #0x38	; 56	'8'
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x0c	; 12
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x28	; 40
	.db #0x28	; 40
	.db #0x10	; 16
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x54	; 84	'T'
	.db #0x54	; 84	'T'
	.db #0x54	; 84	'T'
	.db #0x28	; 40
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x28	; 40
	.db #0x10	; 16
	.db #0x28	; 40
	.db #0x44	; 68	'D'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x44	; 68	'D'
	.db #0x3c	; 60
	.db #0x04	; 4
	.db #0x38	; 56	'8'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x7c	; 124
	.db #0x08	; 8
	.db #0x10	; 16
	.db #0x20	; 32
	.db #0x7c	; 124
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x0e	; 14
	.db #0x08	; 8
	.db #0x30	; 48	'0'
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x0e	; 14
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x08	; 8
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x70	; 112	'p'
	.db #0x10	; 16
	.db #0x0c	; 12
	.db #0x10	; 16
	.db #0x10	; 16
	.db #0x70	; 112	'p'
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x14	; 20
	.db #0x28	; 40
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x00	; 0
	.db #0x3c	; 60
	.db #0x42	; 66	'B'
	.db #0x99	; 153
	.db #0xa1	; 161
	.db #0xa1	; 161
	.db #0x99	; 153
	.db #0x42	; 66	'B'
	.db #0x3c	; 60
;src/vdp.c:58: void vdp_setcolor(unsigned char border, unsigned char background, unsigned char foreground)
;	---------------------------------
; Function vdp_setcolor
; ---------------------------------
_vdp_setcolor::
;src/vdp.c:60: _bright = (foreground & 0x08) ? 1 : 0;
	ld	hl, #4+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x08
	jr	Z,00103$
	ld	c,#0x01
	jr	00104$
00103$:
	ld	c,#0x00
00104$:
	ld	hl,#__bright + 0
	ld	(hl), c
;src/vdp.c:61: fg = foreground & 0x07;
	ld	hl, #4+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	ld	(#_fg + 0),a
;src/vdp.c:62: bg = background & 0x07;
	ld	hl, #3+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	ld	(#_bg + 0),a
;src/vdp.c:63: ULAPORT = (border & 0x07);
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	out	(_ULAPORT),a
	ret
;src/vdp.c:67: void vdp_setborder(unsigned char border)
;	---------------------------------
; Function vdp_setborder
; ---------------------------------
_vdp_setborder::
;src/vdp.c:69: ULAPORT = (border & 0x07);
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	out	(_ULAPORT),a
	ret
;src/vdp.c:73: void vdp_setflash(unsigned char flash)
;	---------------------------------
; Function vdp_setflash
; ---------------------------------
_vdp_setflash::
;src/vdp.c:75: _flash = flash ? 1 : 0;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	or	a, a
	jr	Z,00103$
	ld	c,#0x01
	jr	00104$
00103$:
	ld	c,#0x00
00104$:
	ld	hl,#__flash + 0
	ld	(hl), c
	ret
;src/vdp.c:79: void vdp_setfg(unsigned char foreground)
;	---------------------------------
; Function vdp_setfg
; ---------------------------------
_vdp_setfg::
;src/vdp.c:81: _bright = (foreground & 0x08) ? 1 : 0;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x08
	jr	Z,00103$
	ld	c,#0x01
	jr	00104$
00103$:
	ld	c,#0x00
00104$:
	ld	hl,#__bright + 0
	ld	(hl), c
;src/vdp.c:82: fg = foreground & 0x07;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	ld	(#_fg + 0),a
	ret
;src/vdp.c:86: void vdp_setbg(unsigned char background)
;	---------------------------------
; Function vdp_setbg
; ---------------------------------
_vdp_setbg::
;src/vdp.c:88: bg = background & 0x07;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x07
	ld	(#_bg + 0),a
	ret
;src/vdp.c:92: void vdp_cls()
;	---------------------------------
; Function vdp_cls
; ---------------------------------
_vdp_cls::
;src/vdp.c:97: cx = cy = 0;
	ld	hl,#_cy + 0
	ld	(hl), #0x00
	ld	hl,#_cx + 0
	ld	(hl), #0x00
;src/vdp.c:98: v = (_flash << 7) | (_bright << 6) | (bg << 3) | fg;
	ld	a,(#__flash + 0)
	rrca
	and	a,#0x80
	ld	c,a
	ld	a,(#__bright + 0)
	rrca
	rrca
	and	a,#0xc0
	or	a, c
	ld	c,a
	ld	a,(#_bg + 0)
	rlca
	rlca
	rlca
	and	a,#0xf8
	or	a, c
	ld	hl,#_fg + 0
	or	a,(hl)
	ld	c,a
;src/vdp.c:99: for (c = PIX_BASE; c < (PIX_BASE+6144); c++)
	ld	de,#0x4000
00103$:
;src/vdp.c:100: poke(c, 0);
	ld	l, e
	ld	h, d
	ld	(hl),#0x00
;src/vdp.c:99: for (c = PIX_BASE; c < (PIX_BASE+6144); c++)
	inc	de
	ld	a,d
	sub	a, #0x58
	jr	C,00103$
;src/vdp.c:101: for (c = CT_BASE; c < (CT_BASE+768); c++)
	ld	de,#0x5800
00105$:
;src/vdp.c:102: poke(c, v);
	ld	l, e
	ld	h, d
	ld	(hl),c
;src/vdp.c:101: for (c = CT_BASE; c < (CT_BASE+768); c++)
	inc	de
	ld	a,d
	sub	a, #0x5b
	jr	C,00105$
	ret
;src/vdp.c:106: void vdp_gotoxy(unsigned char x, unsigned char y)
;	---------------------------------
; Function vdp_gotoxy
; ---------------------------------
_vdp_gotoxy::
;src/vdp.c:108: cx = x & 31;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x1f
	ld	(#_cx + 0),a
;src/vdp.c:109: cy = y;
	ld	hl, #3+0
	add	hl, sp
	ld	a, (hl)
	ld	iy,#_cy
	ld	0 (iy),a
;src/vdp.c:110: if (cy > 23) cy = 23;
	ld	a,#0x17
	sub	a, 0 (iy)
	ret	NC
	ld	0 (iy),#0x17
	ret
;src/vdp.c:114: void vdp_gotox(unsigned char x)
;	---------------------------------
; Function vdp_gotox
; ---------------------------------
_vdp_gotox::
;src/vdp.c:116: cx = x & 31;
	ld	hl, #2+0
	add	hl, sp
	ld	a, (hl)
	and	a, #0x1f
	ld	(#_cx + 0),a
	ret
;src/vdp.c:120: void vdp_putchar(unsigned char c)
;	---------------------------------
; Function vdp_putchar
; ---------------------------------
_vdp_putchar::
	call	___sdcc_enter_ix
	push	af
	push	af
;src/vdp.c:124: if (c == 10) {
	ld	a,4 (ix)
	sub	a, #0x0a
	jr	NZ,00111$
;src/vdp.c:125: cx = 0;
	ld	hl,#_cx + 0
	ld	(hl), #0x00
;src/vdp.c:126: ++cy;
	ld	iy,#_cy
	inc	0 (iy)
;src/vdp.c:127: if (cy > 23) {
	ld	a,#0x17
	sub	a, 0 (iy)
	jp	NC,00120$
;src/vdp.c:128: cy = 23;
	ld	0 (iy),#0x17
;src/vdp.c:130: return;
	jp	00120$
00111$:
;src/vdp.c:131: } else if (c == 8) {
	ld	a,4 (ix)
	sub	a, #0x08
	jr	NZ,00112$
;src/vdp.c:132: if (cx == 0) {
	ld	iy,#_cx
	ld	a,0 (iy)
	or	a, a
	jr	NZ,00106$
;src/vdp.c:133: cx = 31;
	ld	0 (iy),#0x1f
;src/vdp.c:134: if (cy > 0) {
	ld	iy,#_cy
	ld	a,0 (iy)
	or	a, a
	jp	Z,00120$
;src/vdp.c:135: --cy;
	dec	0 (iy)
	jp	00120$
00106$:
;src/vdp.c:138: --cx;
	ld	hl, #_cx+0
	dec	(hl)
;src/vdp.c:140: return;
	jp	00120$
00112$:
;src/vdp.c:142: faddr = (c-32)*8;
	ld	a, 4 (ix)
	ld	b, #0x00
	add	a,#0xe0
	ld	l,a
	ld	a,b
	adc	a,#0xff
	ld	h,a
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	(_faddr),hl
;src/vdp.c:143: vaddr = cy << 8;
	ld	a,(#_cy + 0)
	ld	-4 (ix),a
	ld	-3 (ix),#0x00
	ld	a,-4 (ix)
	ld	iy,#_vaddr
	ld	1 (iy),a
	ld	0 (iy),#0x00
;src/vdp.c:144: vaddr = (vaddr & 0x1800) | (vaddr & 0x00E0) << 3 | (vaddr & 0x0700) >> 3;
	ld	c,#0x00
	ld	a,1 (iy)
	and	a, #0x18
	ld	b,a
	ld	a,0 (iy)
	and	a, #0xe0
	ld	l,a
	ld	h,#0x00
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	a,c
	or	a, l
	ld	c,a
	ld	a,b
	or	a, h
	ld	b,a
	ld	e,#0x00
	ld	a,1 (iy)
	and	a, #0x07
	ld	d,a
	ld	a,#0x03
00163$:
	srl	d
	rr	e
	dec	a
	jr	NZ,00163$
	ld	a,c
	or	a, e
	ld	0 (iy),a
	ld	a,b
	or	a, d
	ld	1 (iy),a
;src/vdp.c:145: vaddr = PIX_BASE + vaddr + cx;
	ld	a,0 (iy)
	add	a, #0x00
	ld	e,a
	ld	a,1 (iy)
	adc	a, #0x40
	ld	d,a
	ld	hl,#_cx + 0
	ld	c, (hl)
	ld	b,#0x00
	ld	-2 (ix),c
	ld	-1 (ix),b
	ld	a,e
	ld	hl,#_vaddr
	add	a, -2 (ix)
	ld	(hl),a
	ld	a,d
	adc	a, -1 (ix)
	inc	hl
	ld	(hl),a
;src/vdp.c:146: caddr = CT_BASE + (cy*32) + cx;
	pop	hl
	push	hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	ld	e, l
	ld	a,h
	add	a,#0x58
	ld	d,a
	ld	a,e
	ld	hl,#_caddr
	add	a, c
	ld	(hl),a
	ld	a,d
	adc	a, b
	inc	hl
	ld	(hl),a
;src/vdp.c:148: for (i=0; i < 8; i++) {
	ld	c,#0x00
00118$:
;src/vdp.c:149: poke(vaddr, font[faddr]);
	ld	de,(_vaddr)
	ld	iy,#_font
	push	bc
	ld	bc,(_faddr)
	add	iy, bc
	pop	bc
	ld	a, 0 (iy)
	ld	(de),a
;src/vdp.c:150: vaddr += 256;
	ld	hl,#_vaddr
	ld	a,(hl)
	add	a, #0x00
	ld	(hl),a
	inc	hl
	ld	a,(hl)
	adc	a, #0x01
	ld	(hl),a
;src/vdp.c:151: faddr++;
	ld	iy,#_faddr
	inc	0 (iy)
	jr	NZ,00164$
	inc	1 (iy)
00164$:
;src/vdp.c:148: for (i=0; i < 8; i++) {
	inc	c
	ld	a,c
	sub	a, #0x08
	jr	C,00118$
;src/vdp.c:153: poke(caddr, (_flash << 7) | (_bright << 6) | (bg << 3) | fg);
	ld	bc,(_caddr)
	ld	a,(#__flash + 0)
	rrca
	and	a,#0x80
	ld	e,a
	ld	a,(#__bright + 0)
	rrca
	rrca
	and	a,#0xc0
	or	a, e
	ld	e,a
	ld	a,(#_bg + 0)
	rlca
	rlca
	rlca
	and	a,#0xf8
	or	a, e
	ld	hl,#_fg + 0
	or	a,(hl)
	ld	(bc),a
;src/vdp.c:154: ++cx;
	ld	iy,#_cx
	inc	0 (iy)
;src/vdp.c:155: if (cx > 31) {
	ld	a,#0x1f
	sub	a, 0 (iy)
	jr	NC,00120$
;src/vdp.c:156: cx = 0;
	ld	0 (iy),#0x00
;src/vdp.c:157: ++cy;
	ld	iy,#_cy
	inc	0 (iy)
;src/vdp.c:158: if (cy > 23) {
	ld	a,#0x17
	sub	a, 0 (iy)
	jr	NC,00120$
;src/vdp.c:159: cy = 23;
	ld	0 (iy),#0x17
00120$:
	ld	sp, ix
	pop	ix
	ret
;src/vdp.c:166: void vdp_prints(const char *str)
;	---------------------------------
; Function vdp_prints
; ---------------------------------
_vdp_prints::
;src/vdp.c:169: while ((c = *str++)) vdp_putchar(c);
	pop	de
	pop	bc
	push	bc
	push	de
00101$:
	ld	a,(bc)
	inc	bc
	ld	d,a
	or	a, a
	ret	Z
	push	bc
	push	de
	inc	sp
	call	_vdp_putchar
	inc	sp
	pop	bc
	jr	00101$
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
