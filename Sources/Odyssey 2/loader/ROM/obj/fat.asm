;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW32)
;--------------------------------------------------------
	.module fat
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _GetCluster
	.globl _MMC_Read
	.globl _longfilename
	.globl _sector_buffer
	.globl _dir_entries
	.globl _cluster_mask
	.globl _cluster_size
	.globl _FindDrive
	.globl _NextDirEntry
	.globl _FileOpen
	.globl _FileNextSector
	.globl _FileRead
	.globl _ChangeDirectory
	.globl _IsFat32
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_directory_cluster:
	.ds 2
_entries_per_cluster:
	.ds 2
_fat32:
	.ds 2
_fat_start:
	.ds 8
_data_start:
	.ds 8
_root_directory_cluster:
	.ds 8
_root_directory_start:
	.ds 8
_root_directory_size:
	.ds 8
_fat_number:
	.ds 2
_cluster_size::
	.ds 2
_cluster_mask::
	.ds 8
_fat_size:
	.ds 8
_current_directory_cluster:
	.ds 2
_current_directory_start:
	.ds 2
_partitioncount:
	.ds 2
_dir_entries::
	.ds 2
_sector_buffer::
	.ds 512
_longfilename::
	.ds 260
_NextDirEntry_prevlfn_1_49:
	.ds 2
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;src/fat.c:244: static int prevlfn = 0;
	ld	hl,#0x0000
	ld	(_NextDirEntry_prevlfn_1_49),hl
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/fat.c:88: static int compare(const char *s1, const char *s2,int b)
;	---------------------------------
; Function compare
; ---------------------------------
_compare:
	call	___sdcc_enter_ix
	push	af
	push	af
;src/fat.c:91: for(i=0;i<b;++i)
	ld	c,4 (ix)
	ld	b,5 (ix)
	ld	a,6 (ix)
	ld	-2 (ix),a
	ld	a,7 (ix)
	ld	-1 (ix),a
	ld	de,#0x0000
00105$:
	ld	a,e
	sub	a, 8 (ix)
	ld	a,d
	sbc	a, 9 (ix)
	jp	PO, 00121$
	xor	a, #0x80
00121$:
	jp	P,00103$
;src/fat.c:93: if(*s1++!=*s2++)
	ld	a,(bc)
	ld	-4 (ix),a
	inc	bc
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	a,(hl)
	ld	-3 (ix),a
	inc	-2 (ix)
	jr	NZ,00122$
	inc	-1 (ix)
00122$:
	ld	a,-4 (ix)
	sub	a, -3 (ix)
	jr	Z,00106$
;src/fat.c:94: return(1);
	ld	hl,#0x0001
	jr	00107$
00106$:
;src/fat.c:91: for(i=0;i<b;++i)
	inc	de
	jr	00105$
00103$:
;src/fat.c:96: return(0);
	ld	hl,#0x0000
00107$:
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:101: int FindDrive(void)
;	---------------------------------
; Function FindDrive
; ---------------------------------
_FindDrive::
	call	___sdcc_enter_ix
	ld	hl,#-36
	add	hl,sp
	ld	sp,hl
;src/fat.c:106: fat32=0;
	ld	hl,#0x0000
	ld	(_fat32),hl
;src/fat.c:108: i=5;
	ld	-26 (ix),#0x05
	ld	-25 (ix),#0x00
;src/fat.c:110: while(--i>0)
00103$:
	ld	l,-26 (ix)
	ld	h,-25 (ix)
	dec	hl
	ld	-26 (ix),l
	ld	-25 (ix),h
	xor	a, a
	cp	a, -26 (ix)
	sbc	a, -25 (ix)
	jp	PO, 00214$
	xor	a, #0x80
00214$:
	jp	P,00105$
;src/fat.c:114: if(MMC_Read(0, sector_buffer)) // read MBR
	ld	hl,#_sector_buffer
	push	hl
	ld	hl,#0x0000
	push	hl
	ld	hl,#0x0000
	push	hl
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
	ld	a,l
	or	a, a
	jr	Z,00103$
;src/fat.c:115: i=-1;
	ld	-26 (ix),#0xff
	ld	-25 (ix),#0xff
	jr	00103$
00105$:
;src/fat.c:118: if(!i)	// Did we escape the loop?
	ld	a,-25 (ix)
	or	a,-26 (ix)
	jr	NZ,00107$
;src/fat.c:121: return(0);
	ld	hl,#0x0000
	jp	00140$
00107$:
;src/fat.c:124: boot_sector=0;
	xor	a, a
	ld	-8 (ix),a
	ld	-7 (ix),a
	ld	-6 (ix),a
	ld	-5 (ix),a
	ld	-4 (ix),a
	ld	-3 (ix),a
	ld	-2 (ix),a
	ld	-1 (ix),a
;src/fat.c:125: partitioncount=1;
	ld	hl,#0x0001
	ld	(_partitioncount),hl
;src/fat.c:128: if (compare((const char*)&sector_buffer[0x36], "FAT16   ",8)==0) // check for FAT16
	ld	l, #0x08
	push	hl
	ld	hl,#___str_0
	push	hl
	ld	hl,#(_sector_buffer + 0x0036)
	push	hl
	call	_compare
	pop	af
	pop	af
	pop	af
	ld	-15 (ix),h
	ld	-16 (ix),l
	ld	a,-15 (ix)
	or	a,-16 (ix)
	jr	NZ,00109$
;src/fat.c:129: partitioncount=0;
	ld	hl,#0x0000
	ld	(_partitioncount),hl
00109$:
;src/fat.c:130: if (compare((const char*)&sector_buffer[0x52], "FAT32   ",8)==0) // check for FAT32
	ld	hl,#0x0008
	push	hl
	ld	hl,#___str_1
	push	hl
	ld	hl,#(_sector_buffer + 0x0052)
	push	hl
	call	_compare
	pop	af
	pop	af
	pop	af
	ld	a,h
	or	a,l
	jr	NZ,00111$
;src/fat.c:131: partitioncount=0;
	ld	hl,#0x0000
	ld	(_partitioncount),hl
00111$:
;src/fat.c:135: if(partitioncount)
	ld	iy,#_partitioncount
	ld	a,1 (iy)
	or	a,0 (iy)
	jp	Z,00120$
;src/fat.c:138: struct MasterBootRecord *mbr=(struct MasterBootRecord *)sector_buffer;
	ld	-16 (ix),#<(_sector_buffer)
	ld	-15 (ix),#>(_sector_buffer)
	ld	a,-16 (ix)
	ld	-36 (ix),a
	ld	a,-15 (ix)
	ld	-35 (ix),a
;src/fat.c:140: boot_sector = mbr->Partition[0].startlba;
	ld	a,-36 (ix)
	ld	-16 (ix),a
	ld	a,-35 (ix)
	ld	-15 (ix),a
	ld	l,-16 (ix)
	ld	h,-15 (ix)
	ld	de, #0x01c6
	add	hl, de
	ld	a,(hl)
	ld	-16 (ix),a
	inc	hl
	ld	a,(hl)
	ld	-15 (ix),a
	inc	hl
	ld	a,(hl)
	ld	-14 (ix),a
	inc	hl
	ld	a,(hl)
	ld	-13 (ix),a
	ld	a,-16 (ix)
	ld	-34 (ix),a
	ld	a,-15 (ix)
	ld	-33 (ix),a
	ld	a,-14 (ix)
	ld	-32 (ix),a
	ld	a,-13 (ix)
	ld	-31 (ix),a
	ld	-30 (ix),#0x00
	ld	-29 (ix),#0x00
	ld	-28 (ix),#0x00
	ld	-27 (ix),#0x00
	ld	hl, #28
	add	hl, sp
	ex	de, hl
	ld	hl, #2
	add	hl, sp
	ld	bc, #8
	ldir
;src/fat.c:141: if(mbr->Signature==0x55aa)
	ld	a,-36 (ix)
	ld	-16 (ix),a
	ld	a,-35 (ix)
	ld	-15 (ix),a
	ld	l,-16 (ix)
	ld	h,-15 (ix)
	ld	de, #0x01fe
	add	hl, de
	ld	a,(hl)
	ld	-16 (ix),a
	inc	hl
	ld	a,(hl)
	ld	-15 (ix),a
	ld	a,-16 (ix)
	sub	a, #0xaa
	jr	NZ,00115$
	ld	a,-15 (ix)
	sub	a, #0x55
	jr	NZ,00115$
;src/fat.c:142: boot_sector=SwapBBBB(mbr->Partition[0].startlba);
	ld	hl, #28
	add	hl, sp
	ex	de, hl
	ld	hl, #2
	add	hl, sp
	ld	bc, #8
	ldir
	jr	00116$
00115$:
;src/fat.c:143: else if(mbr->Signature!=0xaa55)
	ld	a,-16 (ix)
	sub	a, #0x55
	jr	NZ,00217$
	ld	a,-15 (ix)
	sub	a, #0xaa
	jr	Z,00116$
00217$:
;src/fat.c:146: return(0);
	ld	hl,#0x0000
	jp	00140$
00116$:
;src/fat.c:149: if (!MMC_Read(boot_sector, sector_buffer)) // read discriptor
	ld	hl, #20
	add	hl, sp
	ex	de, hl
	ld	hl, #28
	add	hl, sp
	ld	bc, #4
	ldir
	ld	hl,#_sector_buffer
	push	hl
	ld	l,-14 (ix)
	ld	h,-13 (ix)
	push	hl
	ld	l,-16 (ix)
	ld	h,-15 (ix)
	push	hl
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
	ld	-16 (ix), l
	ld	a, l
	or	a, a
	jr	NZ,00120$
;src/fat.c:150: return(0);
	ld	hl,#0x0000
	jp	00140$
00120$:
;src/fat.c:155: if (compare(sector_buffer+0x52, "FAT32   ",8)==0) // check for FAT16
	ld	hl,#0x0008
	push	hl
	ld	hl,#___str_1
	push	hl
	ld	hl,#(_sector_buffer + 0x0052)
	push	hl
	call	_compare
	pop	af
	pop	af
	pop	af
	ld	a,h
	or	a,l
	jr	NZ,00124$
;src/fat.c:156: fat32=1;
	ld	hl,#0x0001
	ld	(_fat32),hl
	jr	00125$
00124$:
;src/fat.c:157: else if (compare(sector_buffer+0x36, "FAT16   ",8)!=0) // check for FAT32
	ld	hl,#0x0008
	push	hl
	ld	hl,#___str_0
	push	hl
	ld	hl,#(_sector_buffer + 0x0036)
	push	hl
	call	_compare
	pop	af
	pop	af
	pop	af
	ld	a,h
	or	a,l
	jr	Z,00125$
;src/fat.c:160: return(0);
	ld	hl,#0x0000
	jp	00140$
00125$:
;src/fat.c:163: if (sector_buffer[510] != 0x55 || sector_buffer[511] != 0xaa)  // check signature
	ld	a, (#_sector_buffer + 510)
	sub	a, #0x55
	jr	NZ,00126$
	ld	a, (#_sector_buffer + 511)
	sub	a, #0xaa
	jr	Z,00127$
00126$:
;src/fat.c:164: return(0);
	ld	hl,#0x0000
	jp	00140$
00127$:
;src/fat.c:167: if (sector_buffer[0] != 0xe9 && sector_buffer[0] != 0xeb)
	ld	hl, #_sector_buffer + 0
	ld	c,(hl)
	ld	a,c
	cp	a,#0xe9
	jr	Z,00130$
	sub	a, #0xeb
	jr	Z,00130$
;src/fat.c:168: return(0);
	ld	hl,#0x0000
	jp	00140$
00130$:
;src/fat.c:171: if (sector_buffer[11] != 0x00 || sector_buffer[12] != 0x02)
	ld	a, (#_sector_buffer + 11)
	or	a, a
	jr	NZ,00132$
	ld	a, (#_sector_buffer + 12)
	sub	a, #0x02
	jr	Z,00133$
00132$:
;src/fat.c:172: return(0);
	ld	hl,#0x0000
	jp	00140$
00133$:
;src/fat.c:175: cluster_size = sector_buffer[13];
	ld	a, (#_sector_buffer + 13)
	ld	iy,#_cluster_size
	ld	0 (iy),a
	ld	1 (iy),#0x00
;src/fat.c:178: cluster_mask = cluster_size - 1;
	ld	c,0 (iy)
	ld	b,1 (iy)
	dec	bc
	ld	iy,#_cluster_mask
	ld	0 (iy),c
	ld	1 (iy),b
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
;src/fat.c:180: fat_start = boot_sector + sector_buffer[0x0E] + (sector_buffer[0x0F] << 8); // reserved sector count before FAT table (usually 32 for FAT32)
	ld	a, (#_sector_buffer + 14)
	ld	-24 (ix),a
	ld	-23 (ix),#0x00
	ld	-22 (ix),#0x00
	ld	-21 (ix),#0x00
	ld	-20 (ix),#0x00
	ld	-19 (ix),#0x00
	ld	-18 (ix),#0x00
	ld	-17 (ix),#0x00
	ld	a,-8 (ix)
	add	a, -24 (ix)
	ld	-24 (ix),a
	ld	a,-7 (ix)
	adc	a, -23 (ix)
	ld	-23 (ix),a
	ld	a,-6 (ix)
	adc	a, -22 (ix)
	ld	-22 (ix),a
	ld	a,-5 (ix)
	adc	a, -21 (ix)
	ld	-21 (ix),a
	ld	a,-4 (ix)
	adc	a, -20 (ix)
	ld	-20 (ix),a
	ld	a,-3 (ix)
	adc	a, -19 (ix)
	ld	-19 (ix),a
	ld	a,-2 (ix)
	adc	a, -18 (ix)
	ld	-18 (ix),a
	ld	a,-1 (ix)
	adc	a, -17 (ix)
	ld	-17 (ix),a
	ld	a,(#_sector_buffer + 15)
	ld	-16 (ix), a
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	a,-16 (ix)
	ld	-15 (ix),a
	ld	-16 (ix), #0x00
	ld	-8 (ix), #0x00
	ld	a,-15 (ix)
	ld	-7 (ix),a
	ld	a,-15 (ix)
	rla
	sbc	a, a
	ld	-6 (ix),a
	ld	-5 (ix),a
	ld	-4 (ix),a
	ld	-3 (ix),a
	ld	-2 (ix),a
	ld	-1 (ix),a
	ld	a,-24 (ix)
	ld	hl,#_fat_start
	add	a, -8 (ix)
	ld	(hl),a
	ld	a,-23 (ix)
	adc	a, -7 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-22 (ix)
	adc	a, -6 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-21 (ix)
	adc	a, -5 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-20 (ix)
	adc	a, -4 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-19 (ix)
	adc	a, -3 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-18 (ix)
	adc	a, -2 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-17 (ix)
	adc	a, -1 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:181: fat_number = sector_buffer[0x10];
	ld	a, (#_sector_buffer + 16)
	ld	iy,#_fat_number
	ld	0 (iy),a
	ld	1 (iy),#0x00
;src/fat.c:183: if (fat32)
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jp	Z,00138$
;src/fat.c:185: if (compare((const char*)&sector_buffer[0x52], "FAT32   ",8) != 0) // check file system type
	ld	hl,#0x0008
	push	hl
	ld	hl,#___str_1
	push	hl
	ld	hl,#(_sector_buffer + 0x0052)
	push	hl
	call	_compare
	pop	af
	pop	af
	pop	af
	ld	a,h
	or	a,l
	jr	Z,00136$
;src/fat.c:186: return(0);
	ld	hl,#0x0000
	jp	00140$
00136$:
;src/fat.c:188: dir_entries = cluster_size << 4; // total number of dir entries (16 entries per sector)
	ld	a,(#_cluster_size + 0)
	ld	(#_dir_entries + 0),a
	ld	a,(#_cluster_size + 1)
	ld	iy,#_dir_entries
	ld	1 (iy),a
	ld	a,#0x04+1
	jr	00225$
00224$:
	sla	0 (iy)
	rl	1 (iy)
00225$:
	dec	a
	jr	NZ,00224$
;src/fat.c:189: root_directory_size = cluster_size; // root directory size in sectors
	ld	iy,#_cluster_size
	ld	a,0 (iy)
	ld	-24 (ix),a
	ld	a,1 (iy)
	ld	-23 (ix),a
	ld	-22 (ix),#0x00
	ld	-21 (ix),#0x00
	ld	-20 (ix),#0x00
	ld	-19 (ix),#0x00
	ld	-18 (ix),#0x00
	ld	-17 (ix),#0x00
	ld	de, #_root_directory_size
	ld	hl, #12
	add	hl, sp
	ld	bc, #8
	ldir
;src/fat.c:190: fat_size = sector_buffer[0x24] + ((unsigned long long)sector_buffer[0x25] << 8) + ((unsigned long long)sector_buffer[0x26] << 16) + ((unsigned long long)sector_buffer[0x27] << 24);
	ld	a,(#_sector_buffer + 36)
	ld	-16 (ix),a
	ld	a,(#_sector_buffer + 37)
	ld	-8 (ix), a
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x08
00226$:
	sla	-8 (ix)
	rl	-7 (ix)
	rl	-6 (ix)
	rl	-5 (ix)
	rl	-4 (ix)
	rl	-3 (ix)
	rl	-2 (ix)
	rl	-1 (ix)
	djnz	00226$
	ld	a,-16 (ix)
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	-10 (ix),#0x00
	ld	-9 (ix),#0x00
	ld	a,-16 (ix)
	add	a, -8 (ix)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	adc	a, -7 (ix)
	ld	-15 (ix),a
	ld	a,-14 (ix)
	adc	a, -6 (ix)
	ld	-14 (ix),a
	ld	a,-13 (ix)
	adc	a, -5 (ix)
	ld	-13 (ix),a
	ld	a,-12 (ix)
	adc	a, -4 (ix)
	ld	-12 (ix),a
	ld	a,-11 (ix)
	adc	a, -3 (ix)
	ld	-11 (ix),a
	ld	a,-10 (ix)
	adc	a, -2 (ix)
	ld	-10 (ix),a
	ld	a,-9 (ix)
	adc	a, -1 (ix)
	ld	-9 (ix),a
	ld	a, (#_sector_buffer + 38)
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x10
00228$:
	sla	-8 (ix)
	rl	-7 (ix)
	rl	-6 (ix)
	rl	-5 (ix)
	rl	-4 (ix)
	rl	-3 (ix)
	rl	-2 (ix)
	rl	-1 (ix)
	djnz	00228$
	ld	a,-16 (ix)
	add	a, -8 (ix)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	adc	a, -7 (ix)
	ld	-15 (ix),a
	ld	a,-14 (ix)
	adc	a, -6 (ix)
	ld	-14 (ix),a
	ld	a,-13 (ix)
	adc	a, -5 (ix)
	ld	-13 (ix),a
	ld	a,-12 (ix)
	adc	a, -4 (ix)
	ld	-12 (ix),a
	ld	a,-11 (ix)
	adc	a, -3 (ix)
	ld	-11 (ix),a
	ld	a,-10 (ix)
	adc	a, -2 (ix)
	ld	-10 (ix),a
	ld	a,-9 (ix)
	adc	a, -1 (ix)
	ld	-9 (ix),a
	ld	a, (#_sector_buffer + 39)
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x18
00230$:
	sla	-8 (ix)
	rl	-7 (ix)
	rl	-6 (ix)
	rl	-5 (ix)
	rl	-4 (ix)
	rl	-3 (ix)
	rl	-2 (ix)
	rl	-1 (ix)
	djnz	00230$
	ld	a,-16 (ix)
	ld	hl,#_fat_size
	add	a, -8 (ix)
	ld	(hl),a
	ld	a,-15 (ix)
	adc	a, -7 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-14 (ix)
	adc	a, -6 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-13 (ix)
	adc	a, -5 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-12 (ix)
	adc	a, -4 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-11 (ix)
	adc	a, -3 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-10 (ix)
	adc	a, -2 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-9 (ix)
	adc	a, -1 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:191: data_start = fat_start + (fat_number * fat_size);
	ld	iy,#_fat_number
	ld	a,0 (iy)
	ld	-16 (ix),a
	ld	a,1 (iy)
	ld	-15 (ix),a
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	-10 (ix),#0x00
	ld	-9 (ix),#0x00
	ld	a,(_fat_size + 7)
	push	af
	inc	sp
	ld	a,(_fat_size + 6)
	push	af
	inc	sp
	ld	a,(_fat_size + 5)
	push	af
	inc	sp
	ld	a,(_fat_size + 4)
	push	af
	inc	sp
	ld	a,(_fat_size + 3)
	push	af
	inc	sp
	ld	a,(_fat_size + 2)
	push	af
	inc	sp
	ld	a,(_fat_size + 1)
	push	af
	inc	sp
	ld	a,(_fat_size)
	push	af
	inc	sp
	ld	h,-9 (ix)
	ld	l,-10 (ix)
	push	hl
	ld	h,-11 (ix)
	ld	l,-12 (ix)
	push	hl
	ld	h,-13 (ix)
	ld	l,-14 (ix)
	push	hl
	ld	h,-15 (ix)
	ld	l,-16 (ix)
	push	hl
	ld	hl,#0x0024
	add	hl, sp
	push	hl
	call	__mullonglong
	ld	hl,#18
	add	hl,sp
	ld	sp,hl
	ld	hl,#_data_start
	ld	iy,#_fat_start
	ld	a,0 (iy)
	add	a, -16 (ix)
	ld	(hl),a
	ld	a,1 (iy)
	adc	a, -15 (ix)
	inc	hl
	ld	(hl),a
	ld	a,2 (iy)
	adc	a, -14 (ix)
	inc	hl
	ld	(hl),a
	ld	a,3 (iy)
	adc	a, -13 (ix)
	inc	hl
	ld	(hl),a
	ld	a,4 (iy)
	adc	a, -12 (ix)
	inc	hl
	ld	(hl),a
	ld	a,5 (iy)
	adc	a, -11 (ix)
	inc	hl
	ld	(hl),a
	ld	a,6 (iy)
	adc	a, -10 (ix)
	inc	hl
	ld	(hl),a
	ld	a,7 (iy)
	adc	a, -9 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:192: root_directory_cluster = sector_buffer[0x2C] + ((unsigned long long)sector_buffer[0x2D] << 8) + ((unsigned long long)sector_buffer[0x2E] << 16) + (((unsigned long long)sector_buffer[0x2F] & 0x0F) << 24);
	ld	a,(#_sector_buffer + 44)
	ld	-8 (ix),a
	ld	a,(#_sector_buffer + 45)
	ld	-16 (ix), a
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	-10 (ix),#0x00
	ld	-9 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x08
00232$:
	sla	-16 (ix)
	rl	-15 (ix)
	rl	-14 (ix)
	rl	-13 (ix)
	rl	-12 (ix)
	rl	-11 (ix)
	rl	-10 (ix)
	rl	-9 (ix)
	djnz	00232$
	ld	a,-8 (ix)
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	ld	a,-8 (ix)
	add	a, -16 (ix)
	ld	-16 (ix),a
	ld	a,-7 (ix)
	adc	a, -15 (ix)
	ld	-15 (ix),a
	ld	a,-6 (ix)
	adc	a, -14 (ix)
	ld	-14 (ix),a
	ld	a,-5 (ix)
	adc	a, -13 (ix)
	ld	-13 (ix),a
	ld	a,-4 (ix)
	adc	a, -12 (ix)
	ld	-12 (ix),a
	ld	a,-3 (ix)
	adc	a, -11 (ix)
	ld	-11 (ix),a
	ld	a,-2 (ix)
	adc	a, -10 (ix)
	ld	-10 (ix),a
	ld	a,-1 (ix)
	adc	a, -9 (ix)
	ld	-9 (ix),a
	ld	a, (#_sector_buffer + 46)
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x10
00234$:
	sla	-8 (ix)
	rl	-7 (ix)
	rl	-6 (ix)
	rl	-5 (ix)
	rl	-4 (ix)
	rl	-3 (ix)
	rl	-2 (ix)
	rl	-1 (ix)
	djnz	00234$
	ld	a,-16 (ix)
	add	a, -8 (ix)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	adc	a, -7 (ix)
	ld	-15 (ix),a
	ld	a,-14 (ix)
	adc	a, -6 (ix)
	ld	-14 (ix),a
	ld	a,-13 (ix)
	adc	a, -5 (ix)
	ld	-13 (ix),a
	ld	a,-12 (ix)
	adc	a, -4 (ix)
	ld	-12 (ix),a
	ld	a,-11 (ix)
	adc	a, -3 (ix)
	ld	-11 (ix),a
	ld	a,-10 (ix)
	adc	a, -2 (ix)
	ld	-10 (ix),a
	ld	a,-9 (ix)
	adc	a, -1 (ix)
	ld	-9 (ix),a
	ld	a, (#_sector_buffer + 47)
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	ld	a,-8 (ix)
	and	a, #0x0f
	ld	-8 (ix),a
	ld	-7 (ix),#0x00
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x18
00236$:
	sla	-8 (ix)
	rl	-7 (ix)
	rl	-6 (ix)
	rl	-5 (ix)
	rl	-4 (ix)
	rl	-3 (ix)
	rl	-2 (ix)
	rl	-1 (ix)
	djnz	00236$
	ld	a,-16 (ix)
	ld	hl,#_root_directory_cluster
	add	a, -8 (ix)
	ld	(hl),a
	ld	a,-15 (ix)
	adc	a, -7 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-14 (ix)
	adc	a, -6 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-13 (ix)
	adc	a, -5 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-12 (ix)
	adc	a, -4 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-11 (ix)
	adc	a, -3 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-10 (ix)
	adc	a, -2 (ix)
	inc	hl
	ld	(hl),a
	ld	a,-9 (ix)
	adc	a, -1 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:193: root_directory_start = (root_directory_cluster - 2) * cluster_size + data_start;
	ld	iy,#_root_directory_cluster
	ld	a,0 (iy)
	add	a,#0xfe
	ld	-16 (ix),a
	ld	a,1 (iy)
	adc	a,#0xff
	ld	-15 (ix),a
	ld	a,2 (iy)
	adc	a,#0xff
	ld	-14 (ix),a
	ld	a,3 (iy)
	adc	a,#0xff
	ld	-13 (ix),a
	ld	a,4 (iy)
	adc	a,#0xff
	ld	-12 (ix),a
	ld	a,5 (iy)
	adc	a,#0xff
	ld	-11 (ix),a
	ld	a,6 (iy)
	adc	a,#0xff
	ld	-10 (ix),a
	ld	a,7 (iy)
	adc	a,#0xff
	ld	-9 (ix),a
	ld	h,-17 (ix)
	ld	l,-18 (ix)
	push	hl
	ld	h,-19 (ix)
	ld	l,-20 (ix)
	push	hl
	ld	h,-21 (ix)
	ld	l,-22 (ix)
	push	hl
	ld	h,-23 (ix)
	ld	l,-24 (ix)
	push	hl
	ld	h,-9 (ix)
	ld	l,-10 (ix)
	push	hl
	ld	h,-11 (ix)
	ld	l,-12 (ix)
	push	hl
	ld	h,-13 (ix)
	ld	l,-14 (ix)
	push	hl
	ld	h,-15 (ix)
	ld	l,-16 (ix)
	push	hl
	ld	hl,#0x0024
	add	hl, sp
	push	hl
	call	__mullonglong
	ld	hl,#18
	add	hl,sp
	ld	sp,hl
	ld	a,-16 (ix)
	ld	hl,#_data_start
	ld	iy,#_root_directory_start
	add	a, (hl)
	ld	0 (iy),a
	ld	a,-15 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-14 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-13 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-12 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-11 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-10 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	ld	a,-9 (ix)
	inc	hl
	adc	a, (hl)
	inc	iy
	ld	0 (iy),a
	jp	00139$
00138$:
;src/fat.c:198: dir_entries = sector_buffer[17] + (sector_buffer[18] << 8);
	ld	a, (#_sector_buffer + 17)
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	a,(#_sector_buffer + 18)
	ld	-24 (ix), a
	ld	-24 (ix),a
	ld	-23 (ix),#0x00
	ld	a,-24 (ix)
	ld	-23 (ix),a
	ld	-24 (ix),#0x00
	ld	a,-16 (ix)
	ld	hl,#_dir_entries
	add	a, -24 (ix)
	ld	(hl),a
	ld	a,-15 (ix)
	adc	a, -23 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:199: root_directory_size = ((dir_entries << 5) + 511) >> 9;
	ld	iy,#_dir_entries
	ld	a,0 (iy)
	ld	-16 (ix),a
	ld	a,1 (iy)
	ld	-15 (ix),a
	ld	a,#0x05+1
	jr	00239$
00238$:
	sla	-16 (ix)
	rl	-15 (ix)
00239$:
	dec	a
	jr	NZ,00238$
	ld	a,-16 (ix)
	add	a, #0xff
	ld	-16 (ix),a
	ld	a,-15 (ix)
	adc	a, #0x01
	ld	-15 (ix), a
	srl	a
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	a,-16 (ix)
	ld	iy,#_root_directory_size
	ld	0 (iy),a
	ld	a,-15 (ix)
	ld	1 (iy),a
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
;src/fat.c:202: fat_size = sector_buffer[22] + (sector_buffer[23] << 8);
	ld	a, (#_sector_buffer + 22)
	ld	-16 (ix),a
	ld	-15 (ix),#0x00
	ld	a,(#_sector_buffer + 23)
	ld	-24 (ix), a
	ld	-24 (ix),a
	ld	-23 (ix),#0x00
	ld	a,-24 (ix)
	ld	-23 (ix),a
	ld	-24 (ix),#0x00
	ld	a,-16 (ix)
	add	a, -24 (ix)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	adc	a, -23 (ix)
	ld	-15 (ix),a
	ld	a,-16 (ix)
	ld	iy,#_fat_size
	ld	0 (iy),a
	ld	a,-15 (ix)
	ld	1 (iy),a
	ld	a,-15 (ix)
	rla
	sbc	a, a
	ld	2 (iy),a
	ld	3 (iy),a
	ld	4 (iy),a
	ld	5 (iy),a
	ld	6 (iy),a
	ld	7 (iy),a
;src/fat.c:205: root_directory_start = fat_start + (fat_number * fat_size);
	ld	iy,#_fat_number
	ld	a,0 (iy)
	ld	-16 (ix),a
	ld	a,1 (iy)
	ld	-15 (ix),a
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	-10 (ix),#0x00
	ld	-9 (ix),#0x00
	ld	a,(_fat_size + 7)
	push	af
	inc	sp
	ld	a,(_fat_size + 6)
	push	af
	inc	sp
	ld	a,(_fat_size + 5)
	push	af
	inc	sp
	ld	a,(_fat_size + 4)
	push	af
	inc	sp
	ld	a,(_fat_size + 3)
	push	af
	inc	sp
	ld	a,(_fat_size + 2)
	push	af
	inc	sp
	ld	a,(_fat_size + 1)
	push	af
	inc	sp
	ld	a,(_fat_size)
	push	af
	inc	sp
	ld	h,-9 (ix)
	ld	l,-10 (ix)
	push	hl
	ld	h,-11 (ix)
	ld	l,-12 (ix)
	push	hl
	ld	h,-13 (ix)
	ld	l,-14 (ix)
	push	hl
	ld	h,-15 (ix)
	ld	l,-16 (ix)
	push	hl
	ld	hl,#0x0024
	add	hl, sp
	push	hl
	call	__mullonglong
	ld	hl,#18
	add	hl,sp
	ld	sp,hl
	ld	hl,#_root_directory_start
	ld	iy,#_fat_start
	ld	a,0 (iy)
	add	a, -16 (ix)
	ld	(hl),a
	ld	a,1 (iy)
	adc	a, -15 (ix)
	inc	hl
	ld	(hl),a
	ld	a,2 (iy)
	adc	a, -14 (ix)
	inc	hl
	ld	(hl),a
	ld	a,3 (iy)
	adc	a, -13 (ix)
	inc	hl
	ld	(hl),a
	ld	a,4 (iy)
	adc	a, -12 (ix)
	inc	hl
	ld	(hl),a
	ld	a,5 (iy)
	adc	a, -11 (ix)
	inc	hl
	ld	(hl),a
	ld	a,6 (iy)
	adc	a, -10 (ix)
	inc	hl
	ld	(hl),a
	ld	a,7 (iy)
	adc	a, -9 (ix)
	inc	hl
	ld	(hl),a
;src/fat.c:206: root_directory_cluster = 0; // unused
	xor	a, a
	ld	iy,#_root_directory_cluster
	ld	0 (iy),a
	ld	1 (iy),a
	ld	2 (iy),a
	ld	3 (iy),a
	ld	4 (iy),a
	ld	5 (iy),a
	ld	6 (iy),a
	ld	7 (iy),a
;src/fat.c:209: data_start = root_directory_start + root_directory_size;
	ld	hl,#_root_directory_size
	push	de
	ld	de,#_data_start
	ld	iy,#_root_directory_start
	ld	a,0 (iy)
	add	a, (hl)
	ld	(de),a
	ld	a,1 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,2 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,3 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,4 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,5 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,6 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	ld	a,7 (iy)
	inc	hl
	adc	a, (hl)
	inc	de
	ld	(de),a
	pop	de
00139$:
;src/fat.c:211: ChangeDirectory(0);
	ld	hl,#0x0000
	push	hl
	call	_ChangeDirectory
	pop	af
;src/fat.c:212: return(1);
	ld	hl,#0x0001
00140$:
	ld	sp, ix
	pop	ix
	ret
___str_0:
	.ascii "FAT16   "
	.db 0x00
___str_1:
	.ascii "FAT32   "
	.db 0x00
;src/fat.c:216: int GetCluster(int cluster)
;	---------------------------------
; Function GetCluster
; ---------------------------------
_GetCluster::
	call	___sdcc_enter_ix
	ld	hl,#-10
	add	hl,sp
	ld	sp,hl
;src/fat.c:220: if (fat32)
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	Z,00102$
;src/fat.c:222: sb = cluster >> 7; // calculate sector number containing FAT-link
	ld	c,4 (ix)
	ld	e,5 (ix)
	ld	b,#0x07
00122$:
	sra	e
	rr	c
	djnz	00122$
;src/fat.c:223: i = cluster & 0x7F; // calculate link offsset within sector
	ld	a,4 (ix)
	and	a, #0x7f
	ld	-2 (ix),a
	ld	-1 (ix),#0x00
	jr	00103$
00102$:
;src/fat.c:227: sb = cluster >> 8; // calculate sector number containing FAT-link
	ld	c, 5 (ix)
	ld	a,c
	rlc	a
	sbc	a, a
	ld	e,a
;src/fat.c:228: i = cluster & 0xFF; // calculate link offsset within sector
	ld	a,4 (ix)
	ld	-2 (ix),a
	ld	-1 (ix),#0x00
00103$:
;src/fat.c:231: if (!MMC_Read(fat_start + sb, (unsigned char*)&fat_buffer))
	ld	-10 (ix),c
	ld	-9 (ix),e
	ld	a,e
	rla
	sbc	a, a
	ld	-8 (ix),a
	ld	-7 (ix),a
	ld	-6 (ix),a
	ld	-5 (ix),a
	ld	-4 (ix),a
	ld	-3 (ix),a
	ld	iy,#_fat_start
	ld	a,0 (iy)
	add	a, -10 (ix)
	ld	-10 (ix),a
	ld	a,1 (iy)
	adc	a, -9 (ix)
	ld	-9 (ix),a
	ld	a,2 (iy)
	adc	a, -8 (ix)
	ld	-8 (ix),a
	ld	a,3 (iy)
	adc	a, -7 (ix)
	ld	-7 (ix),a
	ld	a,4 (iy)
	adc	a, -6 (ix)
	ld	-6 (ix),a
	ld	a,5 (iy)
	adc	a, -5 (ix)
	ld	-5 (ix),a
	ld	a,6 (iy)
	adc	a, -4 (ix)
	ld	-4 (ix),a
	ld	a,7 (iy)
	adc	a, -3 (ix)
	ld	-3 (ix),a
	ld	c,-10 (ix)
	ld	b,-9 (ix)
	ld	e,-8 (ix)
	ld	d,-7 (ix)
	ld	hl,#_sector_buffer
	push	hl
	push	de
	push	bc
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
	ld	a,l
	or	a, a
	jr	NZ,00105$
;src/fat.c:232: return(0);
	ld	hl,#0x0000
	jr	00106$
00105$:
;src/fat.c:233: i = fat32 ? SwapBBBB(fat_buffer.fat32[i]) & 0x0FFFFFFF : SwapBB( fat_buffer.fat16[ i ] ); // get FAT link, big-endian
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	Z,00108$
	ld	bc,#_sector_buffer
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	add	hl, hl
	add	hl, hl
	add	hl,bc
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	a, (hl)
	and	a, #0x0f
	jr	00109$
00108$:
	ld	bc,#_sector_buffer
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	add	hl, hl
	add	hl,bc
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
00109$:
;src/fat.c:234: return(i);
	ld	l, c
	ld	h, b
00106$:
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:238: DIRENTRY *NextDirEntry(int prev)
;	---------------------------------
; Function NextDirEntry
; ---------------------------------
_NextDirEntry::
	call	___sdcc_enter_ix
	ld	hl,#-10
	add	hl,sp
	ld	sp,hl
;src/fat.c:248: iDirectorySector = current_directory_start + ( prev >> 4 );
	ld	e,4 (ix)
	ld	d,5 (ix)
	ld	b,#0x04
00139$:
	sra	d
	rr	e
	djnz	00139$
	ld	hl,(_current_directory_start)
	add	hl,de
	ld	-8 (ix),l
	ld	-7 (ix),h
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
;src/fat.c:250: if (( prev & 0x0F ) == 0 ) // first entry in sector, load the sector
	ld	a,4 (ix)
	and	a, #0x0f
	ld	e,a
	ld	d,#0x00
	ld	a,d
	or	a,e
	jr	NZ,00102$
;src/fat.c:252: MMC_Read( iDirectorySector, sector_buffer ); // root directory is linear
	ld	iy,#_sector_buffer
	ld	c,-8 (ix)
	ld	b,-7 (ix)
	ld	l,-6 (ix)
	ld	h,-5 (ix)
	push	de
	push	iy
	push	hl
	push	bc
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
	pop	de
00102$:
;src/fat.c:255: pEntry  = ( DIRENTRY * )sector_buffer;
	ld	bc,#_sector_buffer+0
;src/fat.c:256: pEntry += ( prev & 0xf );
	ld	l, e
	ld	h, d
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl, hl
	add	hl,bc
	inc	sp
	inc	sp
	push	hl
;src/fat.c:258: if ( pEntry->Name[ 0 ] != SLOT_EMPTY && pEntry->Name[ 0 ] != SLOT_DELETED ) // valid entry??
	pop	hl
	push	hl
	ld	c,(hl)
	ld	a,c
	or	a, a
	jp	Z,00111$
	ld	a,c
	sub	a, #0xe5
	jp	Z,00111$
;src/fat.c:260: if (!( pEntry->Attributes & ATTR_VOLUME )) // not a volume
	pop	hl
	push	hl
	ld	de, #0x000b
	add	hl, de
	ld	a,(hl)
	bit	3, a
	jr	NZ,00108$
;src/fat.c:262: if(!prevlfn) longfilename[0]=0;
	ld	iy,#_NextDirEntry_prevlfn_1_49
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	NZ,00104$
	ld	hl,#_longfilename
	ld	(hl),#0x00
00104$:
;src/fat.c:264: prevlfn=0;
	ld	hl,#0x0000
	ld	(_NextDirEntry_prevlfn_1_49),hl
;src/fat.c:267: return(pEntry);
	pop	hl
	push	hl
	jp	00113$
00108$:
;src/fat.c:271: else if ( pEntry->Attributes == ATTR_LFN )	// Do we have a long filename entry?
	sub	a, #0x0f
	jp	NZ,00111$
;src/fat.c:274: int seq = p[ 0 ];
;src/fat.c:275: int offset = (( seq & 0x1f ) -1 ) * 13;
	ld	a,c
	and	a, #0x1f
	ld	c,a
	ld	b,#0x00
	dec	bc
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, hl
	add	hl, bc
;src/fat.c:276: char *o = &longfilename[ offset ];
	ld	bc,#_longfilename
	add	hl,bc
	ld	c,l
	ld	b,h
;src/fat.c:278: *o++ = p[ 0x01 ];
	pop	hl
	push	hl
	inc	hl
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:279: *o++ = p[ 0x03 ];
	pop	hl
	push	hl
	inc	hl
	inc	hl
	inc	hl
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:280: *o++ = p[ 0x05 ];
	pop	hl
	push	hl
	ld	de, #0x0005
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:281: *o++ = p[ 0x07 ];
	pop	hl
	push	hl
	ld	de, #0x0007
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:282: *o++ = p[ 0x09 ];
	pop	hl
	push	hl
	ld	de, #0x0009
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:284: *o++ = p[ 0x0e ];
	pop	hl
	push	hl
	ld	de, #0x000e
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:285: *o++ = p[ 0x10 ];
	pop	hl
	push	hl
	ld	de, #0x0010
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:286: *o++ = p[ 0x12 ];
	pop	hl
	push	hl
	ld	de, #0x0012
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:287: *o++ = p[ 0x14 ];
	pop	hl
	push	hl
	ld	de, #0x0014
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:288: *o++ = p[ 0x16 ];
	pop	hl
	push	hl
	ld	de, #0x0016
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:289: *o++ = p[ 0x18 ];
	pop	hl
	push	hl
	ld	de, #0x0018
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:291: *o++ = p[ 0x1c ];
	pop	hl
	push	hl
	ld	de, #0x001c
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
	inc	bc
;src/fat.c:292: *o++ = p[ 0x1e ];
	pop	hl
	push	hl
	ld	de, #0x001e
	add	hl, de
	ld	a,(hl)
	ld	(bc),a
;src/fat.c:294: prevlfn = 1;
	ld	hl,#0x0001
	ld	(_NextDirEntry_prevlfn_1_49),hl
00111$:
;src/fat.c:300: return (NULL);
	ld	hl,#0x0000
00113$:
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:304: int FileOpen(fileTYPE *file, const char *name)
;	---------------------------------
; Function FileOpen
; ---------------------------------
_FileOpen::
	call	___sdcc_enter_ix
	ld	hl,#-56
	add	hl,sp
	ld	sp,hl
;src/fat.c:306: DIRENTRY      *pEntry = NULL;        // pointer to current entry in sector buffer
	ld	bc,#0x0000
;src/fat.c:311: iDirectoryCluster = current_directory_cluster;
	ld	iy,#_current_directory_cluster
	ld	a,0 (iy)
	ld	-56 (ix),a
	ld	a,1 (iy)
	ld	-55 (ix),a
	ld	-54 (ix),#0x00
	ld	-53 (ix),#0x00
	ld	-52 (ix),#0x00
	ld	-51 (ix),#0x00
	ld	-50 (ix),#0x00
	ld	-49 (ix),#0x00
;src/fat.c:312: iDirectorySector = current_directory_start;
	ld	iy,#_current_directory_start
	ld	a,0 (iy)
	ld	-8 (ix),a
	ld	a,1 (iy)
	ld	-7 (ix),a
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
;src/fat.c:314: while (1)
;src/fat.c:316: for (iEntry = 0; iEntry < dir_entries; iEntry++)
00134$:
	push	bc
	ld	hl, #42
	add	hl, sp
	ex	de, hl
	ld	hl, #50
	add	hl, sp
	ld	bc, #8
	ldir
	pop	bc
	xor	a, a
	ld	-48 (ix),a
	ld	-47 (ix),a
	ld	-46 (ix),a
	ld	-45 (ix),a
	ld	-44 (ix),a
	ld	-43 (ix),a
	ld	-42 (ix),a
	ld	-41 (ix),a
00121$:
	ld	iy,#_dir_entries
	ld	a,0 (iy)
	ld	-32 (ix),a
	ld	a,1 (iy)
	ld	-31 (ix),a
	ld	-30 (ix),#0x00
	ld	-29 (ix),#0x00
	ld	-28 (ix),#0x00
	ld	-27 (ix),#0x00
	ld	-26 (ix),#0x00
	ld	-25 (ix),#0x00
	ld	a,-48 (ix)
	sub	a, -32 (ix)
	ld	a,-47 (ix)
	sbc	a, -31 (ix)
	ld	a,-46 (ix)
	sbc	a, -30 (ix)
	ld	a,-45 (ix)
	sbc	a, -29 (ix)
	ld	a,-44 (ix)
	sbc	a, -28 (ix)
	ld	a,-43 (ix)
	sbc	a, -27 (ix)
	ld	a,-42 (ix)
	sbc	a, -26 (ix)
	ld	a,-41 (ix)
	sbc	a, -25 (ix)
	jp	NC,00111$
;src/fat.c:318: if ((iEntry & 0x0F) == 0) // first entry in sector, load the sector
	ld	a,-48 (ix)
	and	a, #0x0f
	jr	NZ,00102$
;src/fat.c:320: MMC_Read(iDirectorySector++, sector_buffer); // root directory is linear
	ld	hl, #24
	add	hl, sp
	ex	de, hl
	ld	hl, #40
	add	hl, sp
	ld	bc, #8
	ldir
	inc	-16 (ix)
	jr	NZ,00173$
	inc	-15 (ix)
	jr	NZ,00173$
	inc	-14 (ix)
	jr	NZ,00173$
	inc	-13 (ix)
	jr	NZ,00173$
	inc	-12 (ix)
	jr	NZ,00173$
	inc	-11 (ix)
	jr	NZ,00173$
	inc	-10 (ix)
	jr	NZ,00173$
	inc	-9 (ix)
00173$:
	ld	c,-32 (ix)
	ld	b,-31 (ix)
	ld	e,-30 (ix)
	ld	d,-29 (ix)
	ld	hl,#_sector_buffer
	push	hl
	push	de
	push	bc
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
;src/fat.c:321: pEntry = (DIRENTRY*)sector_buffer;
	ld	bc,#_sector_buffer
	jr	00103$
00102$:
;src/fat.c:324: pEntry++;
	ld	hl,#0x0020
	add	hl,bc
	ld	c,l
	ld	b,h
00103$:
;src/fat.c:327: if (pEntry->Name[0] != SLOT_EMPTY && pEntry->Name[0] != SLOT_DELETED) // valid entry??
	ld	a,(bc)
	or	a, a
	jp	Z,00122$
	sub	a, #0xe5
	jp	Z,00122$
;src/fat.c:329: if (!(pEntry->Attributes & (ATTR_VOLUME | ATTR_DIRECTORY))) // not a volume nor directory
	push	bc
	pop	iy
	ld	a,11 (iy)
	and	a, #0x18
	jp	NZ,00122$
;src/fat.c:331: if (compare((const char*)pEntry->Name, name,11) == 0)
	push	bc
	ld	hl,#0x000b
	push	hl
	ld	l,6 (ix)
	ld	h,7 (ix)
	push	hl
	push	bc
	call	_compare
	pop	af
	pop	af
	pop	af
	pop	bc
	ld	a,h
	or	a,l
	jp	NZ,00122$
;src/fat.c:333: file->size = SwapBBBB(pEntry->FileSize); 		// for 68000
	ld	a,4 (ix)
	ld	-16 (ix),a
	ld	a,5 (ix)
	ld	-15 (ix),a
	ld	iy,#0x0004
	ld	e,-16 (ix)
	ld	d,-15 (ix)
	add	iy, de
	ld	l, c
	ld	h, b
	ld	de, #0x001c
	add	hl, de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	hl
	inc	hl
	ld	a,(hl)
	dec	hl
	ld	l,(hl)
	ld	h,a
	ld	0 (iy),e
	ld	1 (iy),d
	ld	2 (iy),l
	ld	3 (iy),h
;src/fat.c:334: file->cluster = SwapBB(pEntry->StartCluster);
	ld	a,-16 (ix)
	add	a, #0x08
	ld	e,a
	ld	a,-15 (ix)
	adc	a, #0x00
	ld	d,a
	push	bc
	pop	iy
	ld	l,26 (iy)
	ld	h,27 (iy)
	ld	-24 (ix),l
	ld	-23 (ix),h
	ld	-22 (ix),#0x00
	ld	-21 (ix),#0x00
	push	de
	push	bc
	ld	hl, #0x0024
	add	hl, sp
	ld	bc, #0x0004
	ldir
	pop	bc
	pop	de
;src/fat.c:335: file->cluster += (fat32 ? ((unsigned long long)(SwapBB(pEntry->HighCluster) & 0x0FFF)) << 16 : 0);
	push	de
	push	bc
	ld	hl, #0x0024
	add	hl, sp
	ex	de, hl
	ld	bc, #0x0004
	ldir
	pop	bc
	pop	de
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	Z,00125$
	ld	l, c
	ld	h, b
	ld	bc, #0x0014
	add	hl, bc
	ld	c,(hl)
	inc	hl
	ld	a, (hl)
	and	a, #0x0f
	ld	-32 (ix), c
	ld	-31 (ix), a
	ld	-30 (ix),#0x00
	ld	-29 (ix),#0x00
	ld	-28 (ix),#0x00
	ld	-27 (ix),#0x00
	ld	-26 (ix),#0x00
	ld	-25 (ix),#0x00
	push	af
	pop	af
	ld	b,#0x10
00177$:
	sla	-32 (ix)
	rl	-31 (ix)
	rl	-30 (ix)
	rl	-29 (ix)
	rl	-28 (ix)
	rl	-27 (ix)
	rl	-26 (ix)
	rl	-25 (ix)
	djnz	00177$
	jr	00126$
00125$:
	xor	a, a
	ld	-32 (ix),a
	ld	-31 (ix),a
	ld	-30 (ix),a
	ld	-29 (ix),a
	ld	-28 (ix),a
	ld	-27 (ix),a
	ld	-26 (ix),a
	ld	-25 (ix),a
00126$:
	ld	a,-24 (ix)
	ld	-24 (ix),a
	ld	a,-23 (ix)
	ld	-23 (ix),a
	ld	a,-22 (ix)
	ld	-22 (ix),a
	ld	a,-21 (ix)
	ld	-21 (ix),a
	ld	-20 (ix),#0x00
	ld	-19 (ix),#0x00
	ld	-18 (ix),#0x00
	ld	-17 (ix),#0x00
	ld	a,-24 (ix)
	add	a, -32 (ix)
	ld	-24 (ix),a
	ld	a,-23 (ix)
	adc	a, -31 (ix)
	ld	-23 (ix),a
	ld	a,-22 (ix)
	adc	a, -30 (ix)
	ld	-22 (ix),a
	ld	a,-21 (ix)
	adc	a, -29 (ix)
	ld	-21 (ix),a
	ld	a,-20 (ix)
	adc	a, -28 (ix)
	ld	-20 (ix),a
	ld	a,-19 (ix)
	adc	a, -27 (ix)
	ld	-19 (ix),a
	ld	a,-18 (ix)
	adc	a, -26 (ix)
	ld	-18 (ix),a
	ld	a,-17 (ix)
	adc	a, -25 (ix)
	ld	-17 (ix),a
	push	de
	ld	hl, #34
	add	hl, sp
	ex	de, hl
	ld	hl, #34
	add	hl, sp
	ld	bc, #4
	ldir
	pop	de
	ld	hl, #0x0020
	add	hl, sp
	ld	bc, #0x0004
	ldir
;src/fat.c:336: file->sector = 0;
	ld	l,-16 (ix)
	ld	h,-15 (ix)
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
	inc	hl
	xor	a, a
	ld	(hl), a
	inc	hl
	ld	(hl), a
;src/fat.c:338: return(1);
	ld	hl,#0x0001
	jp	00123$
00122$:
;src/fat.c:316: for (iEntry = 0; iEntry < dir_entries; iEntry++)
	inc	-48 (ix)
	jp	NZ,00121$
	inc	-47 (ix)
	jp	NZ,00121$
	inc	-46 (ix)
	jp	NZ,00121$
	inc	-45 (ix)
	jp	NZ,00121$
	inc	-44 (ix)
	jp	NZ,00121$
	inc	-43 (ix)
	jp	NZ,00121$
	inc	-42 (ix)
	jp	NZ,00121$
	inc	-41 (ix)
	jp	00121$
00111$:
;src/fat.c:344: if (fat32) // subdirectory is a linked cluster chain
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jp	Z,00119$
;src/fat.c:346: iDirectoryCluster = GetCluster(iDirectoryCluster); // get next cluster in chain
	pop	de
	push	de
	push	bc
	push	de
	call	_GetCluster
	pop	af
	pop	bc
	ld	-56 (ix),l
	ld	-55 (ix),h
	ld	a,h
	rla
	sbc	a, a
	ld	-54 (ix),a
	ld	-53 (ix),a
	ld	-52 (ix),a
	ld	-51 (ix),a
	ld	-50 (ix),a
	ld	-49 (ix),a
;src/fat.c:347: if ((iDirectoryCluster & 0x0FFFFFF8) == 0x0FFFFFF8) // check if end of cluster chain
	ld	a,-56 (ix)
	and	a, #0xf8
	ld	-24 (ix),a
	ld	a,-55 (ix)
	ld	-23 (ix),a
	ld	a,-54 (ix)
	ld	-22 (ix),a
	ld	a,-53 (ix)
	and	a, #0x0f
	ld	-21 (ix),a
	ld	-20 (ix),#0x00
	ld	-19 (ix),#0x00
	ld	-18 (ix),#0x00
	ld	-17 (ix),#0x00
	ld	a,-24 (ix)
	sub	a, #0xf8
	jr	NZ,00180$
	ld	a,-23 (ix)
	inc	a
	jr	NZ,00180$
	ld	a,-22 (ix)
	inc	a
	jr	NZ,00180$
	ld	a,-21 (ix)
	sub	a, #0x0f
	jr	NZ,00180$
	ld	a,-20 (ix)
	or	a, a
	jr	NZ,00180$
	ld	a,-19 (ix)
	or	a, a
	jr	NZ,00180$
	ld	a,-18 (ix)
	or	a, a
	jr	NZ,00180$
	ld	a,-17 (ix)
	or	a, a
	jp	Z,00119$
00180$:
;src/fat.c:350: iDirectorySector = data_start + cluster_size * (iDirectoryCluster - 2); // calculate first sector address of the new cluster
	ld	a,-56 (ix)
	add	a,#0xfe
	ld	-24 (ix),a
	ld	a,-55 (ix)
	adc	a,#0xff
	ld	-23 (ix),a
	ld	a,-54 (ix)
	adc	a,#0xff
	ld	-22 (ix),a
	ld	a,-53 (ix)
	adc	a,#0xff
	ld	-21 (ix),a
	ld	a,-52 (ix)
	adc	a,#0xff
	ld	-20 (ix),a
	ld	a,-51 (ix)
	adc	a,#0xff
	ld	-19 (ix),a
	ld	a,-50 (ix)
	adc	a,#0xff
	ld	-18 (ix),a
	ld	a,-49 (ix)
	adc	a,#0xff
	ld	-17 (ix),a
	ld	iy,#_cluster_size
	ld	a,0 (iy)
	ld	-32 (ix),a
	ld	a,1 (iy)
	ld	-31 (ix),a
	ld	-30 (ix),#0x00
	ld	-29 (ix),#0x00
	ld	-28 (ix),#0x00
	ld	-27 (ix),#0x00
	ld	-26 (ix),#0x00
	ld	-25 (ix),#0x00
	push	bc
	ld	h,-17 (ix)
	ld	l,-18 (ix)
	push	hl
	ld	h,-19 (ix)
	ld	l,-20 (ix)
	push	hl
	ld	h,-21 (ix)
	ld	l,-22 (ix)
	push	hl
	ld	h,-23 (ix)
	ld	l,-24 (ix)
	push	hl
	ld	h,-25 (ix)
	ld	l,-26 (ix)
	push	hl
	ld	h,-27 (ix)
	ld	l,-28 (ix)
	push	hl
	ld	h,-29 (ix)
	ld	l,-30 (ix)
	push	hl
	ld	h,-31 (ix)
	ld	l,-32 (ix)
	push	hl
	ld	hl,#0x0032
	add	hl, sp
	push	hl
	call	__mullonglong
	ld	hl,#18
	add	hl,sp
	ld	sp,hl
	pop	bc
	ld	iy,#_data_start
	ld	a,0 (iy)
	add	a, -24 (ix)
	ld	-40 (ix),a
	ld	a,1 (iy)
	adc	a, -23 (ix)
	ld	-39 (ix),a
	ld	a,2 (iy)
	adc	a, -22 (ix)
	ld	-38 (ix),a
	ld	a,3 (iy)
	adc	a, -21 (ix)
	ld	-37 (ix),a
	ld	a,4 (iy)
	adc	a, -20 (ix)
	ld	-36 (ix),a
	ld	a,5 (iy)
	adc	a, -19 (ix)
	ld	-35 (ix),a
	ld	a,6 (iy)
	adc	a, -18 (ix)
	ld	-34 (ix),a
	ld	a,7 (iy)
	adc	a, -17 (ix)
	ld	-33 (ix),a
	push	bc
	ld	hl, #50
	add	hl, sp
	ex	de, hl
	ld	hl, #18
	add	hl, sp
	ld	bc, #8
	ldir
	pop	bc
	jp	00134$
;src/fat.c:353: break;
00119$:
;src/fat.c:356: return(0);
	ld	hl,#0x0000
00123$:
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:360: int FileNextSector(fileTYPE *file)
;	---------------------------------
; Function FileNextSector
; ---------------------------------
_FileNextSector::
	call	___sdcc_enter_ix
	ld	hl,#-10
	add	hl,sp
	ld	sp,hl
;src/fat.c:366: file->sector++;
	ld	a,4 (ix)
	ld	-10 (ix),a
	ld	a,5 (ix)
	ld	-9 (ix),a
	pop	hl
	push	hl
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	c
	jr	NZ,00109$
	inc	b
	jr	NZ,00109$
	inc	e
	jr	NZ,00109$
	inc	d
00109$:
	pop	hl
	push	hl
	ld	(hl),c
	inc	hl
	ld	(hl),b
	inc	hl
	ld	(hl),e
	inc	hl
	ld	(hl),d
;src/fat.c:369: if ((file->sector&cluster_mask) == 0)
	ld	-8 (ix),c
	ld	-7 (ix),b
	ld	-6 (ix),e
	ld	-5 (ix),d
	ld	-4 (ix),#0x00
	ld	-3 (ix),#0x00
	ld	-2 (ix),#0x00
	ld	-1 (ix),#0x00
	ld	a,-8 (ix)
	ld	iy,#_cluster_mask
	and	a, 0 (iy)
	ld	-8 (ix),a
	ld	a,-7 (ix)
	and	a, 1 (iy)
	ld	-7 (ix),a
	ld	a,-6 (ix)
	and	a, 2 (iy)
	ld	-6 (ix),a
	ld	a,-5 (ix)
	and	a, 3 (iy)
	ld	-5 (ix),a
	ld	a,-4 (ix)
	and	a, 4 (iy)
	ld	-4 (ix),a
	ld	a,-3 (ix)
	and	a, 5 (iy)
	ld	-3 (ix),a
	ld	a,-2 (ix)
	and	a, 6 (iy)
	ld	-2 (ix),a
	ld	a,-1 (ix)
	and	a, 7 (iy)
	ld	-1 (ix), a
	or	a, -2 (ix)
	or	a, -3 (ix)
	or	a, -4 (ix)
	or	a, -5 (ix)
	or	a, -6 (ix)
	or	a, -7 (ix)
	or	a,-8 (ix)
	jr	NZ,00102$
;src/fat.c:370: file->cluster=GetCluster(file->cluster);
	ld	a,-10 (ix)
	add	a, #0x08
	ld	l,a
	ld	a,-9 (ix)
	adc	a, #0x00
	ld	h,a
	push	hl
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	push	bc
	call	_GetCluster
	pop	af
	ld	c,l
	ld	b,h
	pop	hl
	ld	a,b
	rla
	sbc	a, a
	ld	e,a
	ld	d,a
	ld	(hl),c
	inc	hl
	ld	(hl),b
	inc	hl
	ld	(hl),e
	inc	hl
	ld	(hl),d
00102$:
;src/fat.c:372: return(1);
	ld	hl,#0x0001
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:376: int FileRead(fileTYPE *file, unsigned char *pBuffer)
;	---------------------------------
; Function FileRead
; ---------------------------------
_FileRead::
	call	___sdcc_enter_ix
	ld	hl,#-28
	add	hl,sp
	ld	sp,hl
;src/fat.c:380: sb = data_start;                         // start of data in partition
	ld	hl, #18
	add	hl, sp
	ex	de, hl
	ld	hl, #_data_start
	ld	bc, #8
	ldir
;src/fat.c:381: sb += cluster_size * (file->cluster-2);  // cluster offset
	ld	a,4 (ix)
	ld	-2 (ix),a
	ld	a,5 (ix)
	ld	-1 (ix),a
	ld	a,-2 (ix)
	add	a, #0x08
	ld	-20 (ix),a
	ld	a,-1 (ix)
	adc	a, #0x00
	ld	-19 (ix),a
	ld	l,-20 (ix)
	ld	h,-19 (ix)
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	a,c
	add	a,#0xfe
	ld	-18 (ix),a
	ld	a,b
	adc	a,#0xff
	ld	-17 (ix),a
	ld	a,e
	adc	a,#0xff
	ld	-16 (ix),a
	ld	a,d
	adc	a,#0xff
	ld	-15 (ix),a
	ld	hl,#_cluster_size
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	ld	de,#0x0000
	ld	l,-16 (ix)
	ld	h,-15 (ix)
	push	hl
	ld	l,-18 (ix)
	ld	h,-17 (ix)
	push	hl
	push	de
	push	bc
	call	__mullong
	pop	af
	pop	af
	pop	af
	pop	af
	ld	-18 (ix),l
	ld	-17 (ix),h
	ld	-16 (ix),e
	ld	-15 (ix),d
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	a,-10 (ix)
	add	a, -18 (ix)
	ld	-18 (ix),a
	ld	a,-9 (ix)
	adc	a, -17 (ix)
	ld	-17 (ix),a
	ld	a,-8 (ix)
	adc	a, -16 (ix)
	ld	-16 (ix),a
	ld	a,-7 (ix)
	adc	a, -15 (ix)
	ld	-15 (ix),a
	ld	a,-6 (ix)
	adc	a, -14 (ix)
	ld	-14 (ix),a
	ld	a,-5 (ix)
	adc	a, -13 (ix)
	ld	-13 (ix),a
	ld	a,-4 (ix)
	adc	a, -12 (ix)
	ld	-12 (ix),a
	ld	a,-3 (ix)
	adc	a, -11 (ix)
	ld	-11 (ix),a
	ld	hl, #0
	add	hl, sp
	ex	de, hl
	ld	hl, #10
	add	hl, sp
	ld	bc, #8
	ldir
;src/fat.c:382: sb += file->sector & cluster_mask;      // sector offset in cluster
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	-18 (ix),c
	ld	-17 (ix),b
	ld	-16 (ix),e
	ld	-15 (ix),d
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	a,-18 (ix)
	ld	iy,#_cluster_mask
	and	a, 0 (iy)
	ld	-18 (ix),a
	ld	a,-17 (ix)
	and	a, 1 (iy)
	ld	-17 (ix),a
	ld	a,-16 (ix)
	and	a, 2 (iy)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	and	a, 3 (iy)
	ld	-15 (ix),a
	ld	a,-14 (ix)
	and	a, 4 (iy)
	ld	-14 (ix),a
	ld	a,-13 (ix)
	and	a, 5 (iy)
	ld	-13 (ix),a
	ld	a,-12 (ix)
	and	a, 6 (iy)
	ld	-12 (ix),a
	ld	a,-11 (ix)
	and	a, 7 (iy)
	ld	-11 (ix),a
	ld	a,-28 (ix)
	add	a, -18 (ix)
	ld	-18 (ix),a
	ld	a,-27 (ix)
	adc	a, -17 (ix)
	ld	-17 (ix),a
	ld	a,-26 (ix)
	adc	a, -16 (ix)
	ld	-16 (ix),a
	ld	a,-25 (ix)
	adc	a, -15 (ix)
	ld	-15 (ix),a
	ld	a,-24 (ix)
	adc	a, -14 (ix)
	ld	-14 (ix),a
	ld	a,-23 (ix)
	adc	a, -13 (ix)
	ld	-13 (ix),a
	ld	a,-22 (ix)
	adc	a, -12 (ix)
	ld	-12 (ix),a
	ld	a,-21 (ix)
	adc	a, -11 (ix)
	ld	-11 (ix),a
;src/fat.c:386: if (!MMC_Read(sb, pBuffer)) {				// read sector from drive
	ld	c,-18 (ix)
	ld	b,-17 (ix)
	ld	e,-16 (ix)
	ld	d,-15 (ix)
	ld	l,6 (ix)
	ld	h,7 (ix)
	push	hl
	push	de
	push	bc
	call	_MMC_Read
	pop	af
	pop	af
	pop	af
	ld	a, l
	or	a, a
	jr	NZ,00104$
;src/fat.c:387: return 0;
	ld	hl,#0x0000
	jp	00106$
00104$:
;src/fat.c:390: file->sector++;
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	inc	c
	jr	NZ,00116$
	inc	b
	jr	NZ,00116$
	inc	e
	jr	NZ,00116$
	inc	d
00116$:
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	(hl),c
	inc	hl
	ld	(hl),b
	inc	hl
	ld	(hl),e
	inc	hl
	ld	(hl),d
;src/fat.c:393: if ((file->sector & cluster_mask) == 0) {
	ld	-18 (ix),c
	ld	-17 (ix),b
	ld	-16 (ix),e
	ld	-15 (ix),d
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
	ld	-12 (ix),#0x00
	ld	-11 (ix),#0x00
	ld	a,-18 (ix)
	ld	iy,#_cluster_mask
	and	a, 0 (iy)
	ld	-18 (ix),a
	ld	a,-17 (ix)
	and	a, 1 (iy)
	ld	-17 (ix),a
	ld	a,-16 (ix)
	and	a, 2 (iy)
	ld	-16 (ix),a
	ld	a,-15 (ix)
	and	a, 3 (iy)
	ld	-15 (ix),a
	ld	a,-14 (ix)
	and	a, 4 (iy)
	ld	-14 (ix),a
	ld	a,-13 (ix)
	and	a, 5 (iy)
	ld	-13 (ix),a
	ld	a,-12 (ix)
	and	a, 6 (iy)
	ld	-12 (ix),a
	ld	a,-11 (ix)
	and	a, 7 (iy)
	ld	-11 (ix), a
	or	a, -12 (ix)
	or	a, -13 (ix)
	or	a, -14 (ix)
	or	a, -15 (ix)
	or	a, -16 (ix)
	or	a, -17 (ix)
	or	a,-18 (ix)
	jr	NZ,00105$
;src/fat.c:394: file->cluster = GetCluster(file->cluster);
	ld	l,-20 (ix)
	ld	h,-19 (ix)
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	push	bc
	call	_GetCluster
	pop	af
	ld	c,l
	ld	b,h
	ld	a,b
	rla
	sbc	a, a
	ld	e,a
	ld	d,a
	ld	l,-20 (ix)
	ld	h,-19 (ix)
	ld	(hl),c
	inc	hl
	ld	(hl),b
	inc	hl
	ld	(hl),e
	inc	hl
	ld	(hl),d
00105$:
;src/fat.c:397: return 1;
	ld	hl,#0x0001
00106$:
	ld	sp, ix
	pop	ix
	ret
;src/fat.c:415: void ChangeDirectory(DIRENTRY *p)
;	---------------------------------
; Function ChangeDirectory
; ---------------------------------
_ChangeDirectory::
	ld	hl,#-16
	add	hl,sp
	ld	sp,hl
;src/fat.c:417: current_directory_cluster = 0;
	ld	hl,#0x0000
	ld	(_current_directory_cluster),hl
;src/fat.c:419: if(p)
	ld	iy,#18
	add	iy,sp
	ld	a,1 (iy)
	or	a,0 (iy)
	jp	Z,00102$
;src/fat.c:421: current_directory_cluster = SwapBB(p->StartCluster);
	ld	c,0 (iy)
	ld	b,1 (iy)
	ld	l, c
	ld	h, b
	ld	de, #0x001a
	add	hl, de
	ld	a,(hl)
	ld	iy,#_current_directory_cluster
	ld	0 (iy),a
	inc	hl
	ld	a,(hl)
	ld	1 (iy),a
;src/fat.c:422: current_directory_cluster |= fat32 ? ((unsigned long long)(SwapBB(p->HighCluster) & 0x0FFF)) << 16 : 0;
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	Z,00108$
	ld	l, c
	ld	h, b
	ld	de, #0x0014
	add	hl, de
	ld	c,(hl)
	inc	hl
	ld	a, (hl)
	and	a, #0x0f
	ld	b,a
	ld	iy,#8
	add	iy,sp
	ld	0 (iy),c
	ld	1 (iy),b
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
	push	af
	pop	af
	ld	b,#0x10
00128$:
	sla	0 (iy)
	rl	1 (iy)
	rl	2 (iy)
	rl	3 (iy)
	rl	4 (iy)
	rl	5 (iy)
	rl	6 (iy)
	rl	7 (iy)
	djnz	00128$
	jr	00109$
00108$:
	xor	a, a
	ld	iy,#8
	add	iy,sp
	ld	0 (iy),a
	ld	1 (iy),a
	ld	2 (iy),a
	ld	3 (iy),a
	ld	4 (iy),a
	ld	5 (iy),a
	ld	6 (iy),a
	ld	7 (iy),a
00109$:
	ld	a,(#_current_directory_cluster + 0)
	ld	iy,#0
	add	iy,sp
	ld	0 (iy),a
	ld	a,(#_current_directory_cluster + 1)
	ld	iy,#0
	add	iy,sp
	ld	1 (iy),a
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
	ld	a,0 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 0 (iy)
	ld	iy,#0
	add	iy,sp
	ld	0 (iy),a
	ld	a,1 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 1 (iy)
	ld	iy,#0
	add	iy,sp
	ld	1 (iy),a
	ld	a,2 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 2 (iy)
	ld	iy,#0
	add	iy,sp
	ld	2 (iy),a
	ld	a,3 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 3 (iy)
	ld	iy,#0
	add	iy,sp
	ld	3 (iy),a
	ld	a,4 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 4 (iy)
	ld	iy,#0
	add	iy,sp
	ld	4 (iy),a
	ld	a,5 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 5 (iy)
	ld	iy,#0
	add	iy,sp
	ld	5 (iy),a
	ld	a,6 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 6 (iy)
	ld	iy,#0
	add	iy,sp
	ld	6 (iy),a
	ld	a,7 (iy)
	ld	iy,#8
	add	iy,sp
	or	a, 7 (iy)
	ld	iy,#0
	add	iy,sp
	ld	7 (iy),a
	ld	de, #_current_directory_cluster
	ld	hl, #0
	add	hl, sp
	ld	bc, #2
	ldir
00102$:
;src/fat.c:428: dir_entries = cluster_size << 4; // 16 entries per sector
	ld	bc,(_cluster_size)
	sla	c
	rl	b
	sla	c
	rl	b
	sla	c
	rl	b
	sla	c
	rl	b
;src/fat.c:425: if(current_directory_cluster) // subfolder
	ld	iy,#_current_directory_cluster
	ld	a,1 (iy)
	or	a,0 (iy)
	jp	Z,00104$
;src/fat.c:427: current_directory_start = data_start + cluster_size * (current_directory_cluster - 2);
	ld	e,0 (iy)
	ld	d,1 (iy)
	dec	de
	dec	de
	push	bc
	push	de
	ld	hl,(_cluster_size)
	push	hl
	call	__mulint
	pop	af
	pop	af
	pop	bc
	ld	iy,#0
	add	iy,sp
	ld	0 (iy),l
	ld	1 (iy),h
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
	ld	hl,#0
	add	hl,sp
	ld	iy,#_data_start
	ld	a,0 (iy)
	add	a, (hl)
	ld	(hl),a
	ld	a,1 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,2 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,3 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,4 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,5 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,6 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	a,7 (iy)
	inc	hl
	adc	a, (hl)
	ld	(hl),a
	ld	hl, #0+0
	add	hl, sp
	ld	a, (hl)
	ld	(#_current_directory_start + 0),a
	ld	hl, #0+1
	add	hl, sp
	ld	a, (hl)
	ld	(#_current_directory_start + 1),a
;src/fat.c:428: dir_entries = cluster_size << 4; // 16 entries per sector
	ld	(_dir_entries),bc
	jp	00106$
00104$:
;src/fat.c:432: current_directory_cluster = root_directory_cluster;
	ld	hl,(_root_directory_cluster)
	ld	(_current_directory_cluster),hl
;src/fat.c:433: current_directory_start = root_directory_start;
	ld	hl,(_root_directory_start)
	ld	(_current_directory_start),hl
;src/fat.c:434: dir_entries = fat32 ?  cluster_size << 4 : root_directory_size << 4; // 16 entries per sector
	ld	iy,#_fat32
	ld	a,1 (iy)
	or	a,0 (iy)
	jr	Z,00110$
	ld	iy,#0
	add	iy,sp
	ld	0 (iy),c
	ld	1 (iy),b
	ld	2 (iy),#0x00
	ld	3 (iy),#0x00
	ld	4 (iy),#0x00
	ld	5 (iy),#0x00
	ld	6 (iy),#0x00
	ld	7 (iy),#0x00
	jp	00111$
00110$:
	push	af
	ld	a,(#_root_directory_size + 0)
	ld	iy,#2
	add	iy,sp
	ld	0 (iy),a
	ld	a,(#_root_directory_size + 1)
	ld	iy,#2
	add	iy,sp
	ld	1 (iy),a
	ld	a,(#_root_directory_size + 2)
	ld	iy,#2
	add	iy,sp
	ld	2 (iy),a
	ld	a,(#_root_directory_size + 3)
	ld	iy,#2
	add	iy,sp
	ld	3 (iy),a
	ld	a,(#_root_directory_size + 4)
	ld	iy,#2
	add	iy,sp
	ld	4 (iy),a
	ld	a,(#_root_directory_size + 5)
	ld	iy,#2
	add	iy,sp
	ld	5 (iy),a
	ld	a,(#_root_directory_size + 6)
	ld	iy,#2
	add	iy,sp
	ld	6 (iy),a
	ld	a,(#_root_directory_size + 7)
	ld	iy,#2
	add	iy,sp
	ld	7 (iy),a
	pop	af
	ld	b,#0x04
00132$:
	sla	0 (iy)
	rl	1 (iy)
	rl	2 (iy)
	rl	3 (iy)
	rl	4 (iy)
	rl	5 (iy)
	rl	6 (iy)
	rl	7 (iy)
	djnz	00132$
00111$:
	ld	de, #_dir_entries
	ld	hl, #0
	add	hl, sp
	ld	bc, #2
	ldir
00106$:
	ld	hl,#16
	add	hl,sp
	ld	sp,hl
	ret
;src/fat.c:439: int IsFat32()
;	---------------------------------
; Function IsFat32
; ---------------------------------
_IsFat32::
;src/fat.c:441: return(fat32);
	ld	hl,(_fat32)
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
