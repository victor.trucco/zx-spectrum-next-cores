/*
TBBlue / ZX Spectrum Next project

Copyright (c) 2015 Fabio Belavenuto & Victor Trucco

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _HARDWARE_H
#define _HARDWARE_H

__sfr __at 0xE7 SD_CONTROL;
__sfr __at 0xE7 SD_STATUS;
__sfr __at 0xEB SD_DATA;
__sfr __at 0xFE ULAPORT;
__sfr __banked __at 0x243B REG_NUM;
__sfr __banked __at 0x253B REG_VAL;
__sfr __banked __at 0x263B JOYPORT;

/* Keyboard */
__sfr __banked __at 0xFEFE HROW0; // SHIFT,Z,X,C,V
__sfr __banked __at 0xFDFE HROW1; // A,S,D,F,G
__sfr __banked __at 0xFBFE HROW2; // Q,W,E,R,T
__sfr __banked __at 0xF7FE HROW3; // 1,2,3,4,5
__sfr __banked __at 0xEFFE HROW4; // 0,9,8,7,6
__sfr __banked __at 0xDFFE HROW5; // P,O,I,U,Y
__sfr __banked __at 0xBFFE HROW6; // ENTER,L,K,J,H
__sfr __banked __at 0x7FFE HROW7; // SPACE,SYM SHFT,M,N,B

#define peek(A) (*(volatile unsigned char*)(A))
#define poke(A,V) *(volatile unsigned char*)(A)=(V)
#define peek16(A) (*(volatile unsigned int*)(A))
#define poke16(A,V) *(volatile unsigned int*)(A)=(V)


/* RAM pages */
#define RAMPAGE_RAMDIVMMC	0x00
#define RAMPAGE_ROMDIVMMC	0x18
#define RAMPAGE_ROMMF		0x19
#define RAMPAGE_ROMSPECCY	0x1C


#endif /* _HARDWARE_H */